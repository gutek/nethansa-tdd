﻿using System.Collections.Generic;

namespace lib
{
    public class SystemUnderTest
    {
        private readonly IMailService _service;
        public SystemUnderTest(IMailService service)
        {
            _service = service;
        }

        public void Spam(string email)
        {
            _service.Send(email);
        }

        public void Spam(IEnumerable<string> mails)
        {
            foreach (var email in mails)
            {
                _service.Send(email);
            }
        }
    }
}