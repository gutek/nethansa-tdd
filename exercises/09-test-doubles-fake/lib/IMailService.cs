﻿using System;

namespace lib
{
    public interface IMailService
    {
        bool Send(string email);
    }

    // przykladowa implementacja IMailService... nie nalezy
    // sie nie przejmowac
    //public class MailServiceImpl : IMailService
    //{
    //    private static int _count;

    //    public bool Send(string email)
    //    {
    //        if (_count >= 100)
    //        {
    //            return false;
    //        }

    //        // magic

    //        return true;
    //    }
    //}
}
