﻿using System;

namespace lib
{
    public class DynamicService
    {
        private readonly IRandomNumberProvieder<int> _random;

        public DynamicService(IRandomNumberProvieder<int> random)
        {
            _random = random;
        }

        public string Generate(string info)
        {
            var date = DateTime.Now;

            var day = date.Day;

            return info + day;
        }
        
        public string Generate(int max)
        {
            return "Nice!" + _random.Next(max);
        }
    }

    public interface IRandomNumberProvieder<T> where T: struct
    {
        T Next(T max);
    }
}