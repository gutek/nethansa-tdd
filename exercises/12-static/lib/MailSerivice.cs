﻿using System;

namespace lib
{
    public class MailSerivice
    {
        public string SendEmail(string subject, string to)
        {
            subject = MailHelper.EscapeSubject(subject);

            return subject;
        } 
    }

    public class MailHelper
    {
        public static string EscapeSubject(string subject)
        {
            return subject.Replace('a', 'b');
        }
    }
}
