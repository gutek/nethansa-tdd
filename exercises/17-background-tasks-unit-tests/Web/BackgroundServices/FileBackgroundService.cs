﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace Web.BackgroundServices
{
    public interface IFileWriter
    {
        void Write(string basePath, string fileName);
    }

    public class FileWriter : IFileWriter
    {
        public void Write(string basePath, string fileName)
        {
            var path = basePath;
            var file = Path.Combine(path, fileName);

            var msg = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + Environment.NewLine;

            File.AppendAllText(file, msg);
        }
    }

    public class FileBackgroundService : IHostedService, IDisposable
    {
        private readonly IHostingEnvironment _env;
        private readonly IFileWriter _writer;
        private Timer _timer;

        public FileBackgroundService(IHostingEnvironment env, IFileWriter writer)
        {
            _env = env;
            _writer = writer;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            System.Diagnostics.Debug.WriteLine("+++++++++++++++++++Hosted service is staring.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(15));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            System.Diagnostics.Debug.WriteLine("-------------------------------Hosted service is DOING WORK.");

            _writer.Write(_env.WebRootPath, "test.txt");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            System.Diagnostics.Debug.WriteLine("+++++++++++++++++++Hosted service is stopping.");
            
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}