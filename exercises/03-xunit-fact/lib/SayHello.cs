﻿using System;

namespace lib
{
    public class SayHello
    {
        private readonly int _day;
        public SayHello(int day)
        {
            _day = day;
        }

        public string Hi(string name, int age)
        {
            if (age > 81)
            {
                throw new ArgumentException("too old", nameof(age));
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("you're ghost", nameof(name));
            }

            var day = _day;

            if (day % 2 == 0)
            {
                return $"Hi, {name}, today is: {day} and your are {age - day} years old";
            }

            return $"Hi, {name}, today is: {day} and your are {age} yearls old";
        }
    }
}