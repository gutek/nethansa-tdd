﻿using System;

namespace lib.tests
{
    public class FakeMailService : IMailService
    {
        public int Count = 0;
        public bool Send(string email)
        {
            if (Count >= 100)
            {
                return false;
            }

            Count++;

            return true;
        }
    }
}