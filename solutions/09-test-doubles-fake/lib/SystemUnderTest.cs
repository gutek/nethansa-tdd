﻿using System;
using System.Collections.Generic;

namespace lib
{
    public class SystemUnderTest
    {
        private readonly IMailService _service;
        public SystemUnderTest(IMailService service)
        {
            _service = service;
        }

        public void Spam(string email)
        {
            var x = _service.Send(email);

            // ZMIANA W STOSUNKU DO ORGINALNEGO CWICZENIA
            // chodzi o to, ze testach nie powinnismy
            // testowac pod modulu a jedynie dany modul
            if (!x)
            {
                throw new Exception("Limit REACHED");
            }
        }

        public void Spam(IEnumerable<string> mails)
        {
            foreach (var email in mails)
            {
                var x = _service.Send(email);

                // ZMIANA W STOSUNKU DO ORGINALNEGO CWICZENIA
                // chodzi o to, ze w testach nie powinnismy
                // testowac pod modulu a jedynie dany modul
                if (!x)
                {
                    throw new Exception("Limit REACHED");
                }
            }
        }
    }
}