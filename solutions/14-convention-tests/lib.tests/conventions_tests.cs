﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using XUnit;

namespace lib.tests
{
    public class conventions_tests
    {
        private IList<string> get_types(Func<Type,bool> filter)
        {
            return Assembly
                .GetCallingAssembly()
                .GetTypes()
                .Where(filter)
                .Select(x => x.FullName)
                .ToList();
        }

	[Fact]
        public void CheckForMissingVmSuffix()
        {
            var types = get_types(t => t.FullName.StartsWith("lib.ViewModels"));
            var result = new List<string>();
            foreach (var type in types)
            {
                if (!type.EndsWith("Vm"))
                {
                    result.Add(type);
                }
            }
            Assert.Equals(0, result.Count);   
        }
	[Fact]
        public void  CheckForCommandsOutsideNamespace()
        {
            var types = get_types(t => t.FullName.EndsWith("Vm") && !t.FullName.StartsWith("lib.ViewModels"));
            var result = new List<string>();
            foreach (var type in types)
            {
                result.Add(type);
            }

            Assert.Equals(0, result.Count);   
        }

    }
}
