﻿using System;
using System.Linq;
using NSubstitute;
using Xunit;

namespace lib.tests
{
    public class nsubstitute_system_under_test_tests
    {
        private IMailService _service;
        private SystemUnderTest _system;

        public nsubstitute_system_under_test_tests()
        {
            _service = Substitute.For<IMailService>();
            _system = new SystemUnderTest(_service);
        }

        [Fact]
        public void spam_does_not_throw_if_limit_is_not_reached()
        {
            // arrange
            _service
                .Send(Arg.Any<string>())
                .Returns(true);

            // act
            _system.Spam("mail");
        }


        [Fact]
        public void spam_should_call_send()
        {
            // arrange
            _service
                .Send(Arg.Any<string>())
                .Returns(true);

            // act
            _system.Spam("mail");

            // assert
            _service.Received(1).Send(Arg.Any<string>());

        }

        [Fact]
        public void spam_throw_if_limit_is_reached()
        {
            // arrange
            _service
                .Send(Arg.Any<string>())
                .Returns(false);

            // act
            Assert.Throws<Exception>(() => _system.Spam("mail"));
        }

        [Fact]
        public void spam_on_list_does_not_throw_if_limit_is_not_reached()
        {
            // arrange
            _service
                .Send(Arg.Any<string>())
                .Returns(true);

            // act
            _system.Spam(Enumerable.Repeat<string>("test", 100));
        }

        [Fact]
        public void spam_on_list_does_calls_send()
        {
            // arrange
            _service
                .Send(Arg.Any<string>())
                .Returns(true);

            // act
            _system.Spam(Enumerable.Repeat<string>("test", 100));

            // assert
            _service.Received(100).Send(Arg.Any<string>());
        }

        [Fact]
        public void spam_on_list_throw_if_limit_is_reached()
        {
            // arrange
            var list = Enumerable.Repeat<string>("test", 100);
            _service.Send(Arg.Any<string>()).Returns(false);

            // act
            Assert.Throws<Exception>(() => _system.Spam(list));
        }
        
    }
}