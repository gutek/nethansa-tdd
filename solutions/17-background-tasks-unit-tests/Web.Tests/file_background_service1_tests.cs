﻿
using Microsoft.AspNetCore.Hosting;
using NSubstitute;
using Web.BackgroundServices;
using Xunit;

namespace Web.Tests
{
    public class file_background_service_tests
    {
        private IFileWriter _fileWriter;
        private IHostingEnvironment _hosting;
        private FileBackgroundService _background;

        public file_background_service_tests()
        {
            _fileWriter = Substitute.For<IFileWriter>();
            // uwaga KTORY HostingEnvironment!!!!!!
            _hosting = Substitute.For<IHostingEnvironment>();
            _background = new FileBackgroundService(_hosting, _fileWriter);
        }
        [Fact]
        public void should_call_write()
        {
            _background.DoWork(null);

            _fileWriter
                .Received(1)
                .Write(Arg.Any<string>(), Arg.Any<string>());
        }
        [Fact]
        public void should_call_www_root()
        {
            _background.DoWork(null);

            var get = _hosting.Received(1).WebRootPath;
        }
    }
}