﻿using System;
using System.Threading;
using Microsoft.AspNetCore.Hosting;
using NSubstitute;
using Web.BackgroundServices;
using Xunit;

namespace Web.Tests
{
    public class file_background_service1_tests
    {
        private IFileWriter _fileWriter;
        private IHostingEnvironment _hosting;
        private FileBackgroundService1 _background;
        private ITimerWrapper _timer;

        public file_background_service1_tests()
        {
            _fileWriter = Substitute.For<IFileWriter>();
            // uwaga KTORY HostingEnvironment!!!!!!
            _hosting = Substitute.For<IHostingEnvironment>();
            _timer = Substitute.For<ITimerWrapper>();
            _background = new FileBackgroundService1(_hosting, _fileWriter, _timer);
        }

        [Fact]
        public void should_create_timer()
        {
            _background.StartAsync(CancellationToken.None);

            _timer
                .Received(1)
                .Create(_background.DoWork, null, Arg.Any<TimeSpan>(), Arg.Any<TimeSpan>());
        }

        [Fact]
        public void should_change_timer_on_stop()
        {
            _background.StopAsync(CancellationToken.None);

            _timer
                .Received(1)
                .Change(Arg.Any<int>(), Arg.Any<int>());
        }
        [Fact]
        public void timer_should_be_disposed()
        {
            _background.Dispose();

            _timer
                .Received(1)
                .Dispose();
        }

        [Fact]
        public void should_call_write()
        {
            _background.DoWork(null);

            _fileWriter
                .Received(1)
                .Write(Arg.Any<string>(), Arg.Any<string>());
        }
        [Fact]
        public void should_call_www_root()
        {
            _background.DoWork(null);

            var get = _hosting.Received(1).WebRootPath;
        }
    }
}