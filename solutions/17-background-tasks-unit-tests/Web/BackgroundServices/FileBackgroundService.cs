﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace Web.BackgroundServices
{
    public interface IFileWriter
    {
        void Write(string basePath, string fileName);
    }

    public class FileWriter : IFileWriter
    {
        public void Write(string basePath, string fileName)
        {
            var path = basePath;
            var file = Path.Combine(path, fileName);

            var msg = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + Environment.NewLine;

            File.AppendAllText(file, msg);
        }
    }

    public class FileBackgroundService : IHostedService, IDisposable
    {
        private readonly IHostingEnvironment _env;
        private readonly IFileWriter _writer;
        private Timer _timer;

        public FileBackgroundService(IHostingEnvironment env, IFileWriter writer)
        {
            _env = env;
            _writer = writer;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            System.Diagnostics.Debug.WriteLine("+++++++++++++++++++Hostd servie is staring.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(15));

            return Task.CompletedTask;
        }

        // zmiana metody na public
        public void DoWork(object state)
        {
            System.Diagnostics.Debug.WriteLine("-------------------------------Hosted service is DOING WORK.");

            _writer.Write(_env.WebRootPath, "test.txt");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            System.Diagnostics.Debug.WriteLine("+++++++++++++++++++Hosted service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }


    public interface ITimerWrapper : IDisposable
    {
        bool Change(int dueTime, int period);
        void Create(TimerCallback callback, object state, TimeSpan dueTime, TimeSpan period);
    }

    public class TimerWrapper : ITimerWrapper
    {
        private Timer _timer;
        public void Dispose()
        {
            _timer?.Dispose();
        }

        public bool Change(int dueTime, int period)
        {
            _timer?.Change(dueTime, period);

            return true;
        }

        public void Create(TimerCallback callback, object state, TimeSpan dueTime, TimeSpan period)
        {
            _timer = new Timer(callback, state, dueTime, period);
        }
    }

    public class FileBackgroundService1 : IHostedService, IDisposable
    {
        private readonly IHostingEnvironment _env;
        private readonly IFileWriter _writer;
        private readonly ITimerWrapper _timer;

        public FileBackgroundService1(IHostingEnvironment env, IFileWriter writer, ITimerWrapper wrapper)
        {
            _env = env;
            _writer = writer;
            _timer = wrapper;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            System.Diagnostics.Debug.WriteLine("+++++++++++++++++++Hostd servie is staring.");

            _timer.Create(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(15));

            return Task.CompletedTask;
        }

        // zmiana metody na public
        public void DoWork(object state)
        {
            System.Diagnostics.Debug.WriteLine("-------------------------------Hosted service is DOING WORK.");

            _writer.Write(_env.WebRootPath, "test.txt");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            System.Diagnostics.Debug.WriteLine("+++++++++++++++++++Hosted service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}