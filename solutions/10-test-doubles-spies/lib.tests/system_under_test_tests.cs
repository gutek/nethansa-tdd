﻿using System;
using System.Linq;
using Xunit;

namespace lib.tets
{
    public class system_under_test_tests
    {
        private SpyMailService _service;
        private SystemUnderTest _system;

        public system_under_test_tests()
        {
            _service = new SpyMailService();
            _system = new SystemUnderTest(_service);
        }

        [Fact]
        public void spam_does_not_throw_if_limit_is_not_reached()
        {
            _service.Count = 0;
            // act
            _system.Spam("mail");
        }


        [Fact]
        public void spam_should_call_send()
        {
            _service.Count = 0;
            // act
            _system.Spam("mail");

            // assert
            Assert.True(_service.WasCalled);

        }

        [Fact]
        public void spam_throw_if_limit_is_reached()
        {
            _service.Count = 100;

            // act
            Assert.Throws<Exception>(() => _system.Spam("mail"));
        }

        [Fact]
        public void spam_on_list_does_not_throw_if_limit_is_not_reached()
        {
            _service.Count = 0;
            // act
            _system.Spam(Enumerable.Repeat<string>("test", 100));
        }

        [Fact]
        public void spam_on_list_does_calls_send()
        {
            _service.Count = 0;
            
            // act
            _system.Spam(Enumerable.Repeat<string>("test", 100));

            // assert
            Assert.True(_service.WasCalled);
        }

        [Fact]
        public void spam_on_list_throw_if_limit_is_reached()
        {
            // arrange
            var list = Enumerable.Repeat<string>("test", 100);
            _service.Count = 100;

            // act
            Assert.Throws<Exception>(() => _system.Spam(list));
        }
        
    }
}