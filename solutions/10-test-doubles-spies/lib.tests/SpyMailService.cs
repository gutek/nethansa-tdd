﻿using System;

namespace lib.tets
{
    public class SpyMailService : IMailService
    {
        public int Count = 0;
        public bool Send(string email)
        {
            WasCalled = true;
            if (Count >= 100)
            {
                return false;
            }

            Count++;

            return true;
        }

        public bool WasCalled;
    }
}