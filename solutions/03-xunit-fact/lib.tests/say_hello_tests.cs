﻿using System;
using Xunit;

namespace lib.tests
{
    public class say_hello_tests
    {
        // notatka do nazwy metod
        // az sie prosi zrobic: when_age_is_above_81_should_throws_argument_exception
        // ale tutaj jeszcze parametryzacji nie mielismy ;)

        [Fact]
        public void when_age_is_82_should_throw_argument_exception()
        {
            // arrange
            var currentDay = 2;
            var say = new SayHello(currentDay);
            var age = 82;
            var name = "test";

            // act
            Assert.Throws<ArgumentException>(() => say.Hi(name, age));
        }

        [Fact]
        public void when_age_is_81_and_name_is_not_null_should_not_throw()
        {
            // arrange
            var currentDay = 2;
            var say = new SayHello(currentDay);
            var age = 81;
            var name = "test";

            // act
            say.Hi(name, age);
        }

        [Fact]
        public void when_age_is_less_then_81_and_name_is_null_should_throw_argument_exception()
        {
            // arrange
            var currentDay = 2;
            var say = new SayHello(currentDay);
            var age = 81;


            // act
            Assert.Throws<ArgumentException>(() => say.Hi(null, age));
        }


        [Fact]
        public void when_age_is_81_and_name_is_Tomek_and_current_day_is_even_shouldreturn_POSITIVE_information_about_Tomek_age()
        {
            // arrange
            var currentDay = 2;
            var say = new SayHello(currentDay);
            var age = 81;
            var name = "Tomek";
            var expected = string.Format("Hi, {0}, today is: {1} and your are {2} years old", name, currentDay, age - currentDay);

            // act
            var result = say.Hi(name, age);

            Assert.Equal(expected, result);
        }
        [Fact]
        public void when_age_is_81_and_name_is_Tomek_and_current_day_is_odd_should_return_REALISTIC_information_about_Tomek_age()
        {
            // arrange
            var currentDay = 3;
            var say = new SayHello(currentDay);
            var age = 81;
            var name = "Tomek";
            var expected = string.Format("Hi, {0}, today is: {1} and your are {2} years old", name, currentDay, age);

            // act
            var result = say.Hi(name, age);

            Assert.Equal(expected, result);
        }
    }
}