﻿using System;
using Xunit;

namespace lib.tests
{
    public class calculator_tests
    {
        [Fact]
        public void when_adding_1_and_2_should_receive_3()
        {
            // arrange
            var calc = new Calculator();
            var expect = 3;

            // act
            var result = calc.Add(1, 2);

            // assert
            Assert.Equal(expect, result);
        }

        [Fact]
        public void when_adding_1_and_minus_2_then_minus_2_should_be_treated_as_2_and_result_should_still_be_3()
        {
            // arrange
            var calc = new Calculator();
            var expect = 3;

            // act
            var result = calc.Add(1, -2);

            // assert
            Assert.Equal(expect, result);
        }


        [Fact]
        public void when_adding_two_numbers_that_cause_overflow_should_throw_exception()
        {
            // arrange
            var calc = new Calculator();
            
            // act
            Assert.Throws<Exception>(() => calc.Add(1, int.MaxValue));
        }
    }
}