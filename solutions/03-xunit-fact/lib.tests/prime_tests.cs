﻿using Xunit;

namespace lib.tests
{
    public class prime_tests
    {
        [Fact]
        public void when_number_is_1_should_return_false()
        {
            // arrange
            var prime = new Prime();

            // act
            var result = prime.IsPrime(1);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void when_number_is_0_should_return_false()
        {
            // arrange
            var prime = new Prime();

            // act
            var result = prime.IsPrime(0);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void when_number_is_minus_1_should_return_false()
        {
            // arrange
            var prime = new Prime();

            // act
            var result = prime.IsPrime(-1);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void when_number_is_even_and_its_2_should_return_true()
        {
            // arrange
            var prime = new Prime();

            // act
            var result = prime.IsPrime(2);

            // assert
            Assert.True(result);
        }

        [Fact]
        public void when_number_is_even_and_its_not_2_should_return_false()
        {
            // arrange
            var prime = new Prime();

            // act
            var result = prime.IsPrime(4);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void when_number_is_3_should_receive_true()
        {
            // arrange
            var prime = new Prime();

            // act
            var result = prime.IsPrime(3);

            // assert
            Assert.True(result);
        }

        [Fact]
        public void when_number_is_5_should_receive_true()
        {
            // arrange
            var prime = new Prime();

            // act
            var result = prime.IsPrime(5);

            // assert
            Assert.True(result);
        }

        [Fact]
        public void when_number_is_9_should_receive_false()
        {
            // arrange
            var prime = new Prime();

            // act
            var result = prime.IsPrime(9);

            // assert
            Assert.False(result);
        }

        [Fact]
        public void when_number_is_15_should_receive_false()
        {
            // arrange
            var prime = new Prime();

            // act
            var result = prime.IsPrime(15);

            // assert
            Assert.False(result);
        }
    }
}