﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace lib.tests
{
    public class fizz_buzz_tests
    {
        private FizzBuzzRiddle _riddle;

        public fizz_buzz_tests()
        {
            _riddle = new FizzBuzzRiddle();
        }

        [Fact]
        public void when_1_is_passed_1_should_be_returned()
        {
            // arrange
            var num = 1;
            var except = num.ToString();

            // act
            var result = _riddle.Solve(num);

            // assert
            Assert.Equal(except, result);
        }

        [Fact]
        public void when_2_is_passed_2_should_be_returned()
        {
            // arrange
            var num = 2;
            var except = num.ToString();

            // act
            var result = _riddle.Solve(num);

            // assert
            Assert.Equal(except, result);
        }

        [Fact]
        public void when_3_is_passed_Fizz_should_be_returned()
        {
            // arrange
            var num = 3;
            var except = "Fizz";

            // act
            var result = _riddle.Solve(num);

            // assert
            Assert.Equal(except, result);
        }


        [Fact]
        public void when_4_is_passed_4_should_be_returned()
        {
            // arrange
            var num = 4;
            var except = num.ToString();

            // act
            var result = _riddle.Solve(num);

            // assert
            Assert.Equal(except, result);
        }


        [Fact]
        public void when_5_is_passed_Buzz_should_be_returned()
        {
            // arrange
            var num = 5;
            var except = "Buzz";

            // act
            var result = _riddle.Solve(num);

            // assert
            Assert.Equal(except, result);
        }

        [Fact]
        public void when_6_is_passed_Fizz_should_be_returned()
        {
            // arrange
            var num = 6;
            var except = "Fizz";

            // act
            var result = _riddle.Solve(num);

            // assert
            Assert.Equal(except, result);
        }

        [Fact]
        public void when_7_is_passed_7_should_be_returned()
        {
            // arrange
            var num = 7;
            var except = num.ToString();

            // act
            var result = _riddle.Solve(num);

            // assert
            Assert.Equal(except, result);
        }


        [Fact]
        public void when_10_is_passed_Buzz_should_be_returned()
        {
            // arrange
            var num = 10;
            var except = "Buzz";

            // act
            var result = _riddle.Solve(num);

            // assert
            Assert.Equal(except, result);
        }

        [Fact]
        public void when_15_is_passed_FizzBuzz_should_be_returned()
        {
            // arrange
            var num = 15;
            var except = "FizzBuzz";

            // act
            var result = _riddle.Solve(num);

            // assert
            Assert.Equal(except, result);
        }
        [Fact]
        public void when_30_is_passed_FizzBuzz_should_be_returned()
        {
            // arrange
            var num = 30;
            var except = "FizzBuzz";

            // act
            var result = _riddle.Solve(num);

            // assert
            Assert.Equal(except, result);
        }

        // GENERALNE TESTY POWSTALY JUZ PO ZAPROJEKTUWANIU KLASY BY ZWRYFIKOWAC PRZYPADKI OPISANE W 
        // ZADANIU - TESTY od 1 do 100

        [Theory]
        [MemberData("generate_test_data")]
        public void for_all_numbers_that_are_not_multiplications_of_3_or_5_number_should_be_returned(int number)
        {
            // arrange
            var except = number.ToString();

            // act
            var result = _riddle.Solve(number);

            // assert
            Assert.Equal(except, result);
        }

        [Theory]
        [MemberData("generate_test_data2")]
        public void for_all_numbers_that_are_ONLY_multiplications_of_3_Fizz_should_be_returned(int number)
        {
            // arrange
            var except = "Fizz";

            // act
            var result = _riddle.Solve(number);

            // assert
            Assert.Equal(except, result);
        }

        [Theory]
        [MemberData("generate_test_data3")]
        public void for_all_numbers_that_are_ONLY_multiplications_of_5_Buzz_should_be_returned(int number)
        {
            // arrange
            var except = "Buzz";

            // act
            var result = _riddle.Solve(number);

            // assert
            Assert.Equal(except, result);
        }
        [Theory]
        [MemberData("generate_test_data4")]
        public void for_all_numbers_that_are_multiplications_of_3_AND_5_FizzBuzz_should_be_returned(int number)
        {
            // arrange
            var except = "FizzBuzz";

            // act
            var result = _riddle.Solve(number);

            // assert
            Assert.Equal(except, result);
        }


        public static IEnumerable<object[]> generate_test_data
        {
            get
            {
                var result = new List<object[]>();

                for (int i = 1; i <= 100; i++)
                {
                    bool fizz = i % 3 == 0;
                    bool buzz = i % 5 == 0;
                    if (fizz || buzz)
                    {
                        continue;
                    }

                    result.Add(new object[] { i });
                }

                return result;
            }
            
        }

        public static IEnumerable<object[]> generate_test_data2
        {
            get
            {
                var result = new List<object[]>();

                for (int i = 1; i <= 100; i++)
                {
                    bool fizz = i % 3 == 0;
                    bool buzz = i % 5 == 0;
                    if (fizz && !buzz)
                    {
                        result.Add(new object[] { i });
                    }

                    
                }

                return result;
            }
            
        }
        public static IEnumerable<object[]> generate_test_data3
        {
            get
            {
                var result = new List<object[]>();

                for (int i = 1; i <= 100; i++)
                {
                    bool fizz = i % 3 == 0;
                    bool buzz = i % 5 == 0;
                    if (!fizz && buzz)
                    {
                        result.Add(new object[] { i });
                    }

                    
                }

                return result;
            }
            
        }
        public static IEnumerable<object[]> generate_test_data4
        {
            get
            {
                var result = new List<object[]>();

                for (int i = 1; i <= 100; i++)
                {
                    bool fizz = i % 3 == 0;
                    bool buzz = i % 5 == 0;
                    if (fizz && buzz)
                    {
                        result.Add(new object[] { i });
                    }

                    
                }

                return result;
            }
            
        }
    }
}