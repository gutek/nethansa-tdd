﻿namespace lib
{
    public class Mutation
    {
        public int GetType(bool isFirst, bool isNew, bool isNice)
        {
            if (isFirst && !isNew && !isNice)
            {
                return 1;
            }


            if (isFirst && isNew && !isNice)
            {
                return 2;
            }


            if (isFirst && !isNew && isNice)
            {
                return 3;
            }


            if (isFirst && isNew && isNice)
            {
                return 1;
            }


            if (!isFirst && !isNew && !isNice)
            {
                return 2;
            }


            if (!isFirst && isNew && !isNice)
            {
                return 3;
            }


            if (!isFirst && isNew && isNice)
            {
                return 1;
            }

            if (!isFirst && !isNew && isNice)
            {
                return 2;
            }

            return -1;
        }
    }
}