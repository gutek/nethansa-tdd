﻿using Xunit;
using Xunit.Sdk;

namespace lib.tests
{
    public class mutation_tests
    {
        [Theory]
        [InlineData(true, false, false  ,1)]
        [InlineData(true, true, false   ,2)]
        [InlineData(true, false, true   ,3)]
        [InlineData(true, true, true    ,1)]
        [InlineData(false, false, false ,2)]
        [InlineData(false, true, false  ,3)]
        [InlineData(false, true, true   ,1)]
        [InlineData(false, false, true  ,2)]
        public void get_type_with_parameters(bool a, bool b, bool c, int should_expect_value)
        {
            // assert
            var m = new Mutation();

            // act
            var result = m.GetType(a, b, c);

            // assert
            Assert.Equal(should_expect_value, result);
        }
    }
}