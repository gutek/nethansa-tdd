﻿using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace lib.tests
{
    public class prime_tests
    {
        public static IEnumerable<object[]> prime_numbers = new List<object[]>
        {
            new object[] { 3 },
            new object[] { 5 },
            new object[] { 13 },
            new object[] { 17 },
            new object[] { 19 },
            new object[] { 23 },
        };

        public static IEnumerable<object[]> numbers_less_then_2 = new List<object[]>
        {
            new object[] { 1 },
            new object[] { 0 },
            new object[] { -10 },
            new object[] { -23423 },
            new object[] { -3462435 },
            new object[] { -3 }
        };
        
        [Theory]
        [InlineData(4)]
        [InlineData(6)]
        [InlineData(8)]
        [InlineData(10)]
        [InlineData(12)]
        public void when_number_is_even_and_its_not_2_should_return_false(int number)
        {
            // arrange
            var prime = new Prime();

            // act
            var result = prime.IsPrime(number);

            // assert
            Assert.False(result);
        }


        [Theory]
        [MemberData("numbers_less_then_2")]
        public void when_number_is_less_then_2_should_return_false(int number)
        {
            // arrange
            var prime = new Prime();

            // act
            var result = prime.IsPrime(0);

            // assert
            Assert.False(result);
        }

        [Theory]
        [MemberData("prime_numbers")]
        public void when_number_is_odd_and_bigger_then_2_and_is_prime_method_should_return_true(int number)
        {
            // arrange
            var prime = new Prime();

            // act
            var result = prime.IsPrime(number);

            // assert
            Assert.True(result);
        }
    }
}