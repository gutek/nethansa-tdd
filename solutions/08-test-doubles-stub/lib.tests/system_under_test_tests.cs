﻿using System.Linq;
using Xunit;

namespace lib.tests
{
    public class system_under_test_tests
    {
        private StubMailService _service;
        private SystemUnderTest _system;

        public system_under_test_tests()
        {
            _service = new StubMailService();
            _system = new SystemUnderTest(_service);
        }

        [Fact]
        public void spam_does_not_throw()
        {
            // act
            _system.Spam("mail");
        }


        [Fact]
        public void spam_on_list_does_not_throw()
        {
            // act
            _system.Spam(Enumerable.Repeat<string>("test", 10));
        }
    }
}