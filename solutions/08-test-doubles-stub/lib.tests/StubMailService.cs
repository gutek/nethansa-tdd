﻿namespace lib.tests
{
    public class StubMailService : IMailService 
    {
        public bool Send(string email)
        {
            if (email == null)
            {
                return false;
            }

            return true;
        }
    }
}