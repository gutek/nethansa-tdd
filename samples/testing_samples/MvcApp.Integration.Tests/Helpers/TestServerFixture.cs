using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;

namespace MvcApp.Integration.Tests.Helpers
{
    public class TestServerFixture : IDisposable
    {
        public TestServerFixture()
        {
            var path = Assembly.GetExecutingAssembly().Location;
            var folderPath = new FileInfo(path).Directory.FullName;
            var webRoot = Path.Combine(folderPath, "..\\..\\..\\..\\MvcApp\\");
            Server = new TestServer(new WebHostBuilder()
                .UseContentRoot(webRoot)
                .UseEnvironment("Development")
                .UseStartup<Startup>()
            );
        }


        public void Dispose()
        {
            Server?.Dispose();
        }

        public TestServer Server { get; private set; }
    }

    /// <summary>
    /// This should create only create one TestServerFixture for all classes
    /// that needs to use this collection, however, its not true on my
    /// computer.
    /// </summary>
    [CollectionDefinition("ABC")]
    public class MvcAppWithBackgroundTaskCollection : ICollectionFixture<TestServerFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}