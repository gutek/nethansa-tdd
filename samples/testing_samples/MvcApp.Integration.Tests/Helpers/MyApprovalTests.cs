﻿using System;
using System.IO;
using System.Reflection;

namespace MvcApp.Integration.Tests.Helpers
{
    public class MyApprovalTests
    {
        public static bool Verify(string actual, string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();

            var path = Path.GetDirectoryName(assembly.Location);
            var assemblyName = assembly.GetName().Name;

            path = path.Substring(0, path.IndexOf(assemblyName) + assemblyName.Length);
            path = Path.Combine(path, "Approvals");

            var filePath = Path.Combine(path, Path.ChangeExtension(fileName, "txt"));

            if (File.Exists(filePath))
            {
                var expected = File.ReadAllText(filePath);
                return expected.Equals(actual, StringComparison.OrdinalIgnoreCase);
            }

            File.WriteAllText(filePath, actual);

            // fail first
            return false;
        }
    }
}