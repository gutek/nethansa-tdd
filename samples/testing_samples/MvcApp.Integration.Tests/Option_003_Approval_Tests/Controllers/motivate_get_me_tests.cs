﻿using System.Threading.Tasks;
using MvcApp.Integration.Tests.Helpers;
using Xunit;

namespace MvcApp.Integration.Tests.Option_003_Approval_Tests.Controllers
{
    [Collection("ABC")]
    public class motivate_get_me_tests
    {
        private readonly TestServerFixture _fixture;

        public motivate_get_me_tests(TestServerFixture fixture)
        {
            _fixture = fixture;
        }

        private async Task<string> execute(string path)
        {
            var client = _fixture.Server.CreateClient();
            
            var result = await client.GetAsync(path);
            var content = await result.Content.ReadAsStringAsync();

            return content;
        }

        [Fact]
        public async Task should_return_me_view_BTW_IT_ALWAYS_FAILS_FIRST_TIME()
        {
            var result = await execute("/motivate/me");

            var verify = MyApprovalTests.Verify(result, "motivate_me.txt");
            Assert.True(verify);
        }
    }
}