﻿using System.Threading.Tasks;
using MvcApp.Integration.Tests.Helpers;
using Xunit;

namespace MvcApp.Integration.Tests.Option_003_Approval_Tests.Controllers
{
    [Collection("ABC")]
    public class adult_get_index_tests
    {
        private readonly TestServerFixture _fixture;

        public adult_get_index_tests(TestServerFixture fixture)
        {
            _fixture = fixture;
        }

        private async Task<string> execute(string path)
        {
            var client = _fixture.Server.CreateClient();
            
            var result = await client.GetAsync(path);
            var content = await result.Content.ReadAsStringAsync();

            return content;
        }

        [Fact(Skip = "We are using random generator number to get current user that make this test impossible to execute, we will need a way to handle current user age to make this test work")]
        public async Task should_return_index_view_for_adult_BTW_IT_ALWAYS_FAILS_FIRST_TIME()
        {
            var result = await execute("/adult/index");

            var verify = MyApprovalTests.Verify(result, "adult_index_adult.txt");
            Assert.True(verify);
        }
        [Fact(Skip = "We are using random generator number to get current user that make this test impossible to execute, we will need a way to handle current user age to make this test work")]
        public async Task should_intercept_request_for_not_adult_BTW_IT_ALWAYS_FAILS_FIRST_TIME()
        {
            var result = await execute("/adult/index");

            var verify = MyApprovalTests.Verify(result, "adult_index_not_found.txt");
            Assert.True(verify);
        }
    }
}