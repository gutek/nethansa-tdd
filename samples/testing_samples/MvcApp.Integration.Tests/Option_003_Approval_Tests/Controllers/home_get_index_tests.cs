﻿using System.Threading.Tasks;
using MvcApp.Integration.Tests.Helpers;
using Xunit;

namespace MvcApp.Integration.Tests.Option_003_Approval_Tests.Controllers
{
    [Collection("ABC")]
    public class home_get_index_tests
    {
        private readonly TestServerFixture _fixture;

        public home_get_index_tests(TestServerFixture fixture)
        {
            _fixture = fixture;
        }

        private async Task<string> execute(string path)
        {
            var client = _fixture.Server.CreateClient();
            
            var result = await client.GetAsync(path);
            var content = await result.Content.ReadAsStringAsync();

            return content;
        }

        [Fact]
        public async Task should_return_index_view_BTW_IT_ALWAYS_FAILS_FIRST_TIME()
        {
            var result = await execute("/home/index");

            var verify = MyApprovalTests.Verify(result, "home_index.txt");
            Assert.True(verify);
        }
    }
}