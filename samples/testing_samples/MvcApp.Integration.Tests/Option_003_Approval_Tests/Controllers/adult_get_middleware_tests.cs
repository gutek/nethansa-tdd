﻿using System.Threading.Tasks;
using MvcApp.Integration.Tests.Helpers;
using Xunit;

namespace MvcApp.Integration.Tests.Option_003_Approval_Tests.Controllers
{
    [Collection("ABC")]
    public class adult_get_middleware_tests
    {
        private readonly TestServerFixture _fixture;

        public adult_get_middleware_tests(TestServerFixture fixture)
        {
            _fixture = fixture;
        }

        private async Task<string> execute(string path)
        {
            var client = _fixture.Server.CreateClient();
            
            var result = await client.GetAsync(path);
            var content = await result.Content.ReadAsStringAsync();

            return content;
        }

        [Fact]
        public async Task should_return_middleware_view_with_injected_text_BTW_IT_ALWAYS_FAILS_FIRST_TIME()
        {
            var result = await execute("/adult/middleware");

            var verify = MyApprovalTests.Verify(result, "adult_middleware.txt");
            Assert.True(verify);
        }
    }
}