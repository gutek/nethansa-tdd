﻿using System.Threading.Tasks;
using MvcApp.Integration.Tests.Helpers;
using Xunit;

namespace MvcApp.Integration.Tests.Option_003_Approval_Tests.Controllers
{
    [Collection("ABC")]
    public class adult_get_content_tests
    {
        private readonly TestServerFixture _fixture;

        public adult_get_content_tests(TestServerFixture fixture)
        {
            _fixture = fixture;
        }

        private async Task<string> execute(string path)
        {
            var client = _fixture.Server.CreateClient();
            
            var result = await client.GetAsync(path);
            var content = await result.Content.ReadAsStringAsync();

            return content;
        }

        [Fact]
        public async Task should_return_content_view_for_adult_BTW_IT_ALWAYS_FAILS_FIRST_TIME()
        {
            var result = await execute("/adult/19");

            var verify = MyApprovalTests.Verify(result, "adult_content_adult.txt");
            Assert.True(verify);
        }
        [Fact]
        public async Task should_return_not_found_BTW_IT_ALWAYS_FAILS_FIRST_TIME()
        {
            var result = await execute("/adult/17");

            var verify = MyApprovalTests.Verify(result, "adult_content_not_found.txt");
            Assert.True(verify);
        }
    }
}