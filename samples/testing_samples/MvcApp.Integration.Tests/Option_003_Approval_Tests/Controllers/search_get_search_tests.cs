﻿using System.Threading.Tasks;
using MvcApp.Integration.Tests.Helpers;
using Xunit;

namespace MvcApp.Integration.Tests.Option_003_Approval_Tests.Controllers
{
    [Collection("ABC")]
    public class search_get_search_tests
    {
        private readonly TestServerFixture _fixture;

        public search_get_search_tests(TestServerFixture fixture)
        {
            _fixture = fixture;
        }

        private async Task<string> execute(string path)
        {
            var client = _fixture.Server.CreateClient();
            
            var result = await client.GetAsync(path);
            var content = await result.Content.ReadAsStringAsync();

            return content;
        }

        [Fact]
        public async Task should_return_search_view_BTW_IT_ALWAYS_FAILS_FIRST_TIME()
        {
            var result = await execute("/search/search?q=1,10,some_text");

            var verify = MyApprovalTests.Verify(result, "search_search.txt");
            Assert.True(verify);
        }
    }
}