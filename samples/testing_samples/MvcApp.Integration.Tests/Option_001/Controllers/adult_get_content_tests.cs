﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using MvcApp.Integration.Tests.Helpers;
using Xunit;

namespace MvcApp.Integration.Tests.Option_001.Controllers
{

    [Collection("ABC")]
    public class adult_get_content_tests
    {
        private readonly TestServerFixture _fixture;

        public adult_get_content_tests(TestServerFixture fixture)
        {
            _fixture = fixture;
        }


        private async Task<string> execute(string path)
        {
            var client = _fixture.Server.CreateClient();
            var result = await client.GetAsync(path);
            var content = await result.Content.ReadAsStringAsync();

            return content;
        }

        [Fact]
        public async Task for_adult_should_return_not_null_result()
        {
            var result = await execute("/adult/19");

            Assert.NotNull(result);
        }
        [Fact]
        public async Task for_not_adult_should_return_not_null_result()
        {
            var result = await execute("/adult/17");

            Assert.NotNull(result);
        }

        [Fact]
        public async Task for_adult_should_return_200_status_code()
        {
            var client = _fixture.Server.CreateClient();
            var result = await client.GetAsync("/adult/19");
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }
        [Fact]
        public async Task for_not_adult_should_return_404_status_code()
        {
            var client = _fixture.Server.CreateClient();
            var result = await client.GetAsync("/adult/17");
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }
        [Fact]
        public async Task for_adult_should_contains_string_that_this_section_is_only_for_adult()
        {
            var result = await execute("/adult/19");

            Assert.Contains("This section is only for adult!", result);
        }
    }
}