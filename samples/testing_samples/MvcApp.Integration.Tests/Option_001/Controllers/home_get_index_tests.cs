﻿using System.Net;
using System.Threading.Tasks;
using MvcApp.Integration.Tests.Helpers;
using Xunit;

namespace MvcApp.Integration.Tests.Option_001.Controllers
{
    [Collection("ABC")]
    // this class fails, as there is a bug in xUnit test runner
    public class home_get_index_tests
    {
        private readonly TestServerFixture _fixture;

        public home_get_index_tests(TestServerFixture fixture)
        {
            _fixture = fixture;
        }

        private async Task<string> execute(string path)
        {
            var client = _fixture.Server.CreateClient();
            var result = await client.GetAsync(path);
            var content = await result.Content.ReadAsStringAsync();

            return content;
        }

        [Fact]
        public async Task should_return_not_null_result()
        {
            var result = await execute("/home/index");

            Assert.NotNull(result);
        }

        [Fact]
        public async Task should_return_200_status_code()
        {
            var client = _fixture.Server.CreateClient();
            var result = await client.GetAsync("/home/index");
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }
        [Fact]
        public async Task should_contains_string_UT_Demos()
        {
            var result = await execute("/home/index");

            Assert.Contains("UT Demos", result);
        }
    }
}