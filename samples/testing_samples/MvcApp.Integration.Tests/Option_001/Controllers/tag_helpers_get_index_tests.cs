﻿using System.Threading.Tasks;
using AngleSharp.Html.Parser;
using MvcApp.Integration.Tests.Helpers;
using Xunit;

namespace MvcApp.Integration.Tests.Option_001.Controllers
{
    /// <summary>
    /// We are parsing text here with Angel framework
    /// </summary>
    [Collection("ABC")]
    public class tag_helpers_get_index_tests
    {
        private readonly TestServerFixture _fixture;

        public tag_helpers_get_index_tests(TestServerFixture fixture)
        {
            _fixture = fixture;
        }

        private async Task<string> execute(string path)
        {
            var client = _fixture.Server.CreateClient();
            var result = await client.GetAsync(path);
            var content = await result.Content.ReadAsStringAsync();

            return content;
        }

        [Fact]
        public async Task page_should_have_biger_text_created()
        {
            var result = await execute("/taghelpers");
            var parser = new HtmlParser();
            var parsed = await parser.ParseDocumentAsync(result);

            var elements = parsed.QuerySelectorAll("p[style=\"font-size: 50px\"]");

            Assert.NotEmpty(elements);
        }
    }
}