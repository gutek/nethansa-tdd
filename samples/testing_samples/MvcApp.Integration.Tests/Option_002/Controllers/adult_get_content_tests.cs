﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace MvcApp.Integration.Tests.Option_002.Controllers
{

    /// <summary>
    /// We are using WebApplicationFactory - this gives a bit more options and
    /// takes configuration from our head
    /// </summary>
    public class adult_get_content_tests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        private HttpClient _client;

        public adult_get_content_tests(WebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }


        private async Task<string> execute(string path)
        {
            var result = await _client.GetAsync(path);
            var content = await result.Content.ReadAsStringAsync();

            return content;
        }

        [Fact]
        public async Task for_adult_should_return_not_null_result()
        {
            var result = await execute("/adult/19");

            Assert.NotNull(result);
        }
        [Fact]
        public async Task for_not_adult_should_return_not_null_result()
        {
            var result = await execute("/adult/17");

            Assert.NotNull(result);
        }

        [Fact]
        public async Task for_adult_should_return_200_status_code()
        {

            var result = await _client.GetAsync("/adult/19");
            result.EnsureSuccessStatusCode();
        }
        [Fact]
        public async Task for_not_adult_should_return_404_status_code()
        {

            var result = await _client.GetAsync("/adult/17");
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }
        [Fact]
        public async Task for_adult_should_contains_string_that_this_section_is_only_for_adult()
        {
            var result = await execute("/adult/19");

            Assert.Contains("This section is only for adult!", result);
        }
    }
}