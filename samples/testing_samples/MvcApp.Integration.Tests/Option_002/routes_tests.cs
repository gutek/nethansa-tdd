﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace MvcApp.Integration.Tests.Option_002
{
    /// <summary>
    /// We are using WebApplicationFactory - this gives a bit more options and
    /// takes configuration from our head
    /// </summary>
    public class routes_tests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public routes_tests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Theory]
        [InlineData("/")]
        [InlineData("/home/index")]
        [InlineData("/adult/20")]
        [InlineData("/TagHelpers")]
        [InlineData("/TagHelpers/index")]
        [InlineData("/motivate/me")]
        [InlineData("/search/search?q=1,20,smth")]
        public async Task get_returns_success_and_html(string path)
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync(path);

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            Assert.Equal("text/html; charset=utf-8",
                response.Content.Headers.ContentType.ToString());
        }
    }
}