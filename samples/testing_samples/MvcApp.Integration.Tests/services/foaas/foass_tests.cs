﻿using System.Threading.Tasks;
using MvcApp.Services;
using Xunit;

namespace MvcApp.Integration.Tests.services.foaas
{
    public class foass_tests
    {
        private readonly IFoaasClient _foaasClient;

        public foass_tests()
        {
            _foaasClient = new FoaasClient();
        }

        [Fact]
        public async Task FuckingOffShouldWork()
        {
            var res = await _foaasClient.Off("Joker", "Batman");
            Assert.NotNull(res);
            Assert.Equal("Fuck off, Joker.", res.Message);
            Assert.Equal("- Batman", res.Subtitle);
        }

        [Fact]
        public async Task FuckingYouShouldWork()
        {
            var res = await _foaasClient.You("Harpo", "Groucho");
            Assert.NotNull(res);
            Assert.Equal("Fuck you, Harpo.", res.Message);
            Assert.Equal("- Groucho", res.Subtitle);
        }

        [Fact]
        public async Task FuckingThatShouldWork()
        {
            var res = await _foaasClient.That("Moses");
            Assert.NotNull(res);
            Assert.Equal("Fuck that.", res.Message);
            Assert.Equal("- Moses", res.Subtitle);
        }

        [Fact]
        public async Task FuckingThisShouldWork()
        {
            var res = await _foaasClient.This("Jesus H. Christ");
            Assert.NotNull(res);
            Assert.Equal("Fuck this.", res.Message);
            Assert.Equal("- Jesus H. Christ", res.Subtitle);
        }

        [Fact]
        public async Task FuckingEverythingShouldWork()
        {
            var res = await _foaasClient.Everything("John F. Kennedy");
            Assert.NotNull(res);
            Assert.Equal("Fuck everything.", res.Message);
            Assert.Equal("- John F. Kennedy", res.Subtitle);
        }

        [Fact]
        public async Task FuckingEveryoneShouldWork()
        {
            var res = await _foaasClient.Everyone("Bumblebee");
            Assert.NotNull(res);
            Assert.Equal("Everyone can go and fuck off.", res.Message);
            Assert.Equal("- Bumblebee", res.Subtitle);
        }

        [Fact]
        public async Task FuckingDonutShouldWork()
        {
            var res = await _foaasClient.Donut("Hillary", "Bill");
            Assert.NotNull(res);
            Assert.Equal("Hillary, go and take a flying fuck at a rolling donut.", res.Message);
            Assert.Equal("- Bill", res.Subtitle);
        }

        [Fact]
        public async Task FuckingShakespeareShouldWork()
        {
            var res = await _foaasClient.Shakespeare("Wolverine", "Professor X");
            Assert.NotNull(res);
            Assert.Equal(
                "Wolverine, Thou clay-brained guts, thou knotty-pated fool, thou whoreson obscene greasy tallow-catch!",
                res.Message);
            Assert.Equal("- Professor X", res.Subtitle);
        }

        [Fact]
        public async Task FuckingLinusShouldWork()
        {
            var res = await _foaasClient.Linus("Lucy", "Charlie Brown");
            Assert.NotNull(res);
            Assert.Equal(
                "Lucy, there aren't enough swear-words in the English language, so now I'll have to call you perkeleen vittupää just to express my disgust and frustration with this crap.",
                res.Message);
            Assert.Equal("- Charlie Brown", res.Subtitle);
        }

        [Fact]
        public async Task FuckingKingShouldWork()
        {
            var res = await _foaasClient.King("Lucy", "Ricki");
            Assert.NotNull(res);
            Assert.Equal(
                "Oh fuck off, just really fuck off you total dickface. Christ, Lucy, you are fucking thick.", res.Message);
            Assert.Equal("- Ricki", res.Subtitle);
        }

        [Fact]
        public async Task FuckingPinkShouldWork()
        {
            var res = await _foaasClient.Pink("Simba");
            Assert.NotNull(res);
            Assert.Equal("Well, fuck me pink.", res.Message);
            Assert.Equal("- Simba", res.Subtitle);
        }

        [Fact]
        public async Task FuckingLifeShouldWork()
        {
            var res = await _foaasClient.Life("Gandhi");
            Assert.NotNull(res);
            Assert.Equal("Fuck my life.", res.Message);
            Assert.Equal("- Gandhi", res.Subtitle);
        }

        [Fact]
        public async Task FuckingChainsawShouldWork()
        {
            var res = await _foaasClient.Chainsaw("Jim", "Dr. McCoy");
            Assert.NotNull(res);
            Assert.Equal("Fuck me gently with a chainsaw, Jim. Do I look like Mother Teresa?", res.Message);
            Assert.Equal("- Dr. McCoy", res.Subtitle);
        }

        [Fact]
        public async Task FuckingOutsideShouldWork()
        {
            var res = await _foaasClient.Outside("BigBrother", "TheWorld");
            Assert.NotNull(res);
            Assert.Equal("BigBrother, why don't you go outside and play hide-and-go-fuck-yourself?", res.Message);
            Assert.Equal("- TheWorld", res.Subtitle);
        }

        [Fact]
        public async Task FuckingThingShouldWork()
        {
            var res = await _foaasClient.Thing("pineapples", "Chris");
            Assert.NotNull(res);
            Assert.Equal("Fuck pineapples.", res.Message);
            Assert.Equal("- Chris", res.Subtitle);
        }

        [Fact]
        public async Task FuckingThanksShouldWork()
        {
            var res = await _foaasClient.Thanks("Abraham Lincoln");
            Assert.NotNull(res);
            Assert.Equal("Fuck you very much.", res.Message);
            Assert.Equal("- Abraham Lincoln", res.Subtitle);
        }

        [Fact]
        public async Task FuckingFlyingShouldWork()
        {
            var res = await _foaasClient.Flying("Barack Obama");
            Assert.NotNull(res);
            Assert.Equal("I don't give a flying fuck.", res.Message);
            Assert.Equal("- Barack Obama", res.Subtitle);
        }

        [Fact]
        public async Task FuckingFascinatingShouldWork()
        {
            var res = await _foaasClient.Fascinating("Marie Curie");
            Assert.NotNull(res);
            Assert.Equal("Fascinating story, in what chapter do you shut the fuck up?", res.Message);
            Assert.Equal("- Marie Curie", res.Subtitle);
        }

        [Fact]
        public async Task FuckingMadisonShouldWork()
        {
            var res = await _foaasClient.Madison("Doctor", "Rose");
            Assert.NotNull(res);
            Assert.Equal(
                "What you've just said is one of the most insanely idiotic things I have ever heard, Doctor. At no point in your rambling, incoherent response were you even close to anything that could be considered a rational thought. Everyone in this room is now dumber for having listened to it. I award you no points Doctor, and may God have mercy on your soul.",
                res.Message);
            Assert.Equal("- Rose", res.Subtitle);
        }

        [Fact]
        public async Task FuckingCoolShouldWork()
        {
            var res = await _foaasClient.Cool("Luke");
            Assert.NotNull(res);
            Assert.Equal("Cool story, bro.", res.Message);
            Assert.Equal("- Luke", res.Subtitle);
        }

        [Fact]
        public async Task FuckingFieldShouldWork()
        {
            var res = await _foaasClient.Field("Yo Momma", "John", "A Book, Chapter 2.");
            Assert.NotNull(res);
            Assert.Equal(
                "And Yo Momma said unto John, 'Verily, cast thine eyes upon the field in which I grow my fucks', and John gave witness unto the field, and saw that it was barren.",
                res.Message);
            Assert.Equal("- A Book, Chapter 2.", res.Subtitle);
        }

        [Fact]
        public async Task FuckingNuggetShouldWork()
        {
            var res = await _foaasClient.Nugget("Bill", "Ben");
            Assert.NotNull(res);
            Assert.Equal("Well Bill, aren't you a shining example of a rancid fuck-nugget.", res.Message);
            Assert.Equal("- Ben", res.Subtitle);
        }

        [Fact]
        public async Task FuckingYodaShouldWork()
        {
            var res = await _foaasClient.Yoda("Luke", "Yoda");
            Assert.NotNull(res);
            Assert.Equal("Fuck off, you must, Luke.", res.Message);
            Assert.Equal("- Yoda", res.Subtitle);
        }

        [Fact]
        public async Task FuckingBallmerShouldWork()
        {
            var res = await _foaasClient.Ballmer("Someone", "Something", "Ballmer");
            Assert.NotNull(res);
            Assert.Equal(
                "Fucking Someone is a fucking pussy. I'm going to fucking bury that guy, I have done it before, and I will do it again. I'm going to fucking kill Something.",
                res.Message);
            Assert.Equal("- Ballmer", res.Subtitle);
        }

        [Fact]
        public async Task FuckingWhatShouldWork()
        {
            var res = await _foaasClient.What("Frank");
            Assert.NotNull(res);
            Assert.Equal("What the fuck‽", res.Message);
            Assert.Equal("- Frank", res.Subtitle);
        }

        [Fact]
        public async Task FuckingBecauseShouldWork()
        {
            var res = await _foaasClient.Because("Tina");
            Assert.NotNull(res);
            Assert.Equal("Why? Because fuck you, that's why.", res.Message);
            Assert.Equal("- Tina", res.Subtitle);
        }

        [Fact]
        public async Task FuckingCaniuseShouldWork()
        {
            var res = await _foaasClient.Caniuse("Hammer", "Bob the Builder");
            Assert.NotNull(res);
            Assert.Equal("Can you use Hammer? Fuck no!", res.Message);
            Assert.Equal("- Bob the Builder", res.Subtitle);
        }

        [Fact]
        public async Task FuckingByeShouldWork()
        {
            var res = await _foaasClient.Bye("Claire");
            Assert.NotNull(res);
            Assert.Equal("Fuckity bye!", res.Message);
            Assert.Equal("- Claire", res.Subtitle);
        }

        [Fact]
        public async Task FuckingDiabetesShouldWork()
        {
            var res = await _foaasClient.Diabetes("Charley");
            Assert.NotNull(res);
            Assert.Equal("I'd love to stop and chat to you but I'd rather have type 2 diabetes.", res.Message);
            Assert.Equal("- Charley", res.Subtitle);
        }

        [Fact]
        public async Task FuckingBusShouldWork()
        {
            var res = await _foaasClient.Bus("Laura", "James");
            Assert.NotNull(res);
            Assert.Equal("Christ on a bendy-bus, Laura, don't be such a fucking faff-arse.", res.Message);
            Assert.Equal("- James", res.Subtitle);
        }

        [Fact]
        public async Task FuckingXmasShouldWork()
        {
            var res = await _foaasClient.Xmas("Clive", "Your Mother");
            Assert.NotNull(res);
            Assert.Equal("Merry Fucking Christmas, Clive.", res.Message);
            Assert.Equal("- Your Mother", res.Subtitle);
        }

        [Fact]
        public async Task FuckingBdayShouldWork()
        {
            var res = await _foaasClient.Bday("Grant", "The Crew");
            Assert.NotNull(res);
            Assert.Equal("Happy Fucking Birthday, Grant.", res.Message);
            Assert.Equal("- The Crew", res.Subtitle);
        }

        [Fact]
        public async Task FuckingAwesomeShouldWork()
        {
            var res = await _foaasClient.Awesome("Macklemore");
            Assert.NotNull(res);
            Assert.Equal("This is Fucking Awesome.", res.Message);
            Assert.Equal("- Macklemore", res.Subtitle);
        }

        [Fact]
        public async Task FuckingTuckerShouldWork()
        {
            var res = await _foaasClient.Tucker("Malcolm Tucker");
            Assert.NotNull(res);
            Assert.Equal("Come the fuck in or fuck the fuck off.", res.Message);
            Assert.Equal("- Malcolm Tucker", res.Subtitle);
        }

        [Fact]
        public async Task FuckingBucketShouldWork()
        {
            var res = await _foaasClient.Bucket("Richard Dickson");
            Assert.NotNull(res);
            Assert.Equal("Please choke on a bucket of cocks.", res.Message);
            Assert.Equal("- Richard Dickson", res.Subtitle);
        }

        [Fact]
        public async Task FuckingFamilyShouldWork()
        {
            var res = await _foaasClient.Family("Jezza");
            Assert.NotNull(res);
            Assert.Equal("Fuck you, your whole family, your pets, and your feces.", res.Message);
            Assert.Equal("- Jezza", res.Subtitle);
        }

        [Fact]
        public async Task FuckingShutupShouldWork()
        {
            var res = await _foaasClient.Shutup("Jeff", "Gary");
            Assert.NotNull(res);
            Assert.Equal("Jeff, shut the fuck up.", res.Message);
            Assert.Equal("- Gary", res.Subtitle);
        }

        [Fact]
        public async Task FuckingZaynShouldWork()
        {
            var res = await _foaasClient.Zayn("Zayn");
            Assert.NotNull(res);
            Assert.Equal("Ask me if I give a motherfuck ?!!", res.Message);
            Assert.Equal("- Zayn", res.Subtitle);
        }

        [Fact]
        public async Task FuckingKeepCalmShouldWork()
        {
            var res = await _foaasClient.KeepCalm("Cry me a fast flowing river", "Dustin Timberland");
            Assert.NotNull(res);
            Assert.Equal("Keep the fuck calm and Cry me a fast flowing river!", res.Message);
            Assert.Equal("- Dustin Timberland", res.Subtitle);
        }

        [Fact]
        public async Task FuckingDoSomethingShouldWork()
        {
            var res = await _foaasClient.DoSomething("Get", "Washing", "Mother");
            Assert.NotNull(res);
            Assert.Equal("Get the fucking Washing!", res.Message);
            Assert.Equal("- Mother", res.Subtitle);
        }

        [Fact]
        public async Task FuckingMorninShouldWork()
        {
            var res = await _foaasClient.Mornin("Monday");
            Assert.NotNull(res);
            Assert.Equal("Happy fuckin' mornin'!", res.Message);
            Assert.Equal("- Monday", res.Subtitle);
        }

        [Fact]
        public async Task FuckingThumbsShouldWork()
        {
            var res = await _foaasClient.Thumbs("This Guy", "Me");
            Assert.NotNull(res);
            Assert.Equal("Who has two thumbs and doesn't give a fuck? This Guy.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingRetardShouldWork()
        {
            var res = await _foaasClient.Retard("Josh");
            Assert.NotNull(res);
            Assert.Equal("You Fucktard!", res.Message);
            Assert.Equal("- Josh", res.Subtitle);
        }

        [Fact]
        public async Task FuckingGreedShouldWork()
        {
            var res = await _foaasClient.Greed("Cash", "BigCat");
            Assert.NotNull(res);
            Assert.Equal(
                "The point is, ladies and gentleman, that cash -- for lack of a better word -- is good. Cash is right. Cash works. Cash clarifies, cuts through, and captures the essence of the evolutionary spirit. Cash, in all of its forms -- Cash for life, for money, for love, knowledge -- has marked the upward surge of mankind.",
                res.Message);
            Assert.Equal("- BigCat", res.Subtitle);
        }

        [Fact]
        public async Task FuckingAnywayShouldWork()
        {
            var res = await _foaasClient.Anyway("Microsoft", "Someone Else");
            Assert.NotNull(res);
            Assert.Equal("Who the fuck are you anyway, Microsoft, why are you stirring up so much trouble, and, who pays you?", res.Message);
            Assert.Equal("- Someone Else", res.Subtitle);
        }

        [Fact]
        public async Task FuckingBackShouldWork()
        {
            var res = await _foaasClient.Back("You", "Me");
            Assert.NotNull(res);
            Assert.Equal("You, back the fuck off.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingBagShouldWork()
        {
            var res = await _foaasClient.Bag("Me");
            Assert.NotNull(res);
            Assert.Equal("Eat a bag of fucking dicks.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingBlackadderShouldWork()
        {
            var res = await _foaasClient.Blackadder("You", "Me");
            Assert.NotNull(res);
            Assert.Equal("You, your head is as empty as a eunuch’s underpants. Fuck off!", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingBravoMikeShouldWork()
        {
            var res = await _foaasClient.BravoMike("Frank", "Me");
            Assert.NotNull(res);
            Assert.Equal("Bravo mike, Frank.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingCocksplatShouldWork()
        {
            var res = await _foaasClient.Cocksplat("Frank", "Me");
            Assert.NotNull(res);
            Assert.Equal("Fuck off Frank, you worthless cocksplat", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingDaltonShouldWork()
        {
            var res = await _foaasClient.Dalton("You", "Me");
            Assert.NotNull(res);
            Assert.Equal("You: A fucking problem solving super-hero.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingDeraadtShouldWork()
        {
            var res = await _foaasClient.Deraadt("Frank", "Me");
            Assert.NotNull(res);
            Assert.Equal("Frank you are being the usual slimy hypocritical asshole... You may have had value ten years ago, but people will see that you don't anymore.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingGolfFoxtrotYankeeShouldWork()
        {
            var res = await _foaasClient.GolfFoxtrotYankee("Frank", "Me");
            Assert.NotNull(res);
            Assert.Equal("Golf foxtrot yankee, Frank.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingGiveShouldWork()
        {
            var res = await _foaasClient.Give("Me");
            Assert.NotNull(res);
            Assert.Equal("I give zero fucks.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingHorseShouldWork()
        {
            var res = await _foaasClient.Horse("Me");
            Assert.NotNull(res);
            Assert.Equal("Fuck you and the horse you rode in on.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingFuckOffShouldWork()
        {
            var res = await _foaasClient.FuckingOff("Frank", "Me");
            Assert.NotNull(res);
            Assert.Equal("Fucking fuck off, Frank.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingKeepShouldWork()
        {
            var res = await _foaasClient.Keep("Frank", "Me");
            Assert.NotNull(res);
            Assert.Equal("Frank: Fuck off. And when you get there, fuck off from there too. Then fuck off some more. Keep fucking off until you get back here. Then fuck off again.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingLookShouldWork()
        {
            var res = await _foaasClient.Look("Frank", "Me");
            Assert.NotNull(res);
            Assert.Equal("Frank, do I look like I give a fuck?", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingLookingShouldWork()
        {
            var res = await _foaasClient.Looking("Me");
            Assert.NotNull(res);
            Assert.Equal("Looking for a fuck to give.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingMaybeShouldWork()
        {
            var res = await _foaasClient.Maybe("Me");
            Assert.NotNull(res);
            Assert.Equal("Maybe. Maybe not. Maybe fuck yourself.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingMeShouldWork()
        {
            var res = await _foaasClient.Me("Me");
            Assert.NotNull(res);
            Assert.Equal("Fuck me.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingNoShouldWork()
        {
            var res = await _foaasClient.No("Me");
            Assert.NotNull(res);
            Assert.Equal("No fucks given.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingOffWithShouldWork()
        {
            var res = await _foaasClient.OffWith("that", "Me");
            Assert.NotNull(res);
            Assert.Equal("Fuck off with that", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingParticularShouldWork()
        {
            var res = await _foaasClient.Particular("Sausage", "Me");
            Assert.NotNull(res);
            Assert.Equal("Fuck this Sausage in particular.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingProblemShouldWork()
        {
            var res = await _foaasClient.Problem("Frank", "Me");
            Assert.NotNull(res);
            Assert.Equal("What the fuck is your problem Frank?", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingPulpShouldWork()
        {
            var res = await _foaasClient.Pulp("Japanese", "Me");
            Assert.NotNull(res);
            Assert.Equal("Japanese, motherfucker, do you speak it?", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingRidiculousShouldWork()
        {
            var res = await _foaasClient.Ridiculous("Me");
            Assert.NotNull(res);
            Assert.Equal("That's fucking ridiculous", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task ReadingTheFuckingManualShouldWork()
        {
            var res = await _foaasClient.ReadTheManual("Me");
            Assert.NotNull(res);
            Assert.Equal("Read the fucking manual!", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingSakeShouldWork()
        {
            var res = await _foaasClient.Sake("Me");
            Assert.NotNull(res);
            Assert.Equal("For Fuck's sake!", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingShitShouldWork()
        {
            var res = await _foaasClient.Shit("Me");
            Assert.NotNull(res);
            Assert.Equal("Fuck this shit!", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingSingleShouldWork()
        {
            var res = await _foaasClient.Single("Me");
            Assert.NotNull(res);
            Assert.Equal("Not a single fuck was given.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingThinkShouldWork()
        {
            var res = await _foaasClient.Think("Frank", "Me");
            Assert.NotNull(res);
            Assert.Equal("Frank, you think I give a fuck?", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingThinkingShouldWork()
        {
            var res = await _foaasClient.Thinking("Frank", "Me");
            Assert.NotNull(res);
            Assert.Equal("Frank, what the fuck were you actually thinking?", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingTooShouldWork()
        {
            var res = await _foaasClient.Too("Me");
            Assert.NotNull(res);
            Assert.Equal("Thanks, fuck you too.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }

        [Fact]
        public async Task FuckingZeroShouldWork()
        {
            var res = await _foaasClient.Zero("Me");
            Assert.NotNull(res);
            Assert.Equal("Zero, thats the number of fucks I give.", res.Message);
            Assert.Equal("- Me", res.Subtitle);
        }
    }
}