﻿using System;

namespace SharedLib.Models
{
    public class UserView
    {
        public Guid Id { get; set; }
        public string Address { get; set; }
    }
}