﻿using System;

namespace SharedLib.Models
{
    /// <summary>
    /// Do not test POCO objects, do not test Props, Get/Set even if you want to null validation on set.
    ///
    /// There is however an exception, if POCO starts to have behaviour its not POCO anymore.
    ///
    /// Do test its behaviours.
    ///
    /// THIS IS AN EXAMPLE OF POCO with SIMPLE validation
    /// </summary>
    public class Report
    {
        private int _age;
        private string _name;

        public Report(string name, int age)
        {
            Age = age;
            Name = name;
        }

        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int Age
        {
            get { return _age; }
            set
            {
                if (value < 0)
                {
                    throw new Exception("value to low");
                }
                _age = value;
            }
        }
    }
}