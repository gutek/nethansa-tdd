﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MvcApp.Infrastructure.BackgroundTasks;
using MvcApp.Infrastructure.Middlewares;
using MvcApp.Infrastructure.ModelBinders.SearchQuery;
using MvcApp.Infrastructure.RouteConstraints;
using MvcApp.Services;

namespace MvcApp
{
    /// <summary>
    /// Normally we do not test Startup class, what we can however test
    /// if our services have been registered.
    /// </summary>
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // disabling
            //services.Configure<CookiePolicyOptions>(options =>
            //{
            //    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
            //    options.CheckConsentNeeded = context => true;
            //    options.MinimumSameSitePolicy = SameSiteMode.None;
            //});


            services.AddMvc(options =>
            {
                options.ModelBinderProviders.Insert(0, new SearchQueryModelBinderProvider());

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.Configure<RouteOptions>(otp =>
            {
                otp.ConstraintMap.Add("adult", typeof(AdultRouteConstraint));
            });


            services.AddHostedService<FileBackgroundService>();
            services.AddTransient<ITimerWrapper, TimerWrapper>();
            services.AddTransient<IFileWriter, FileWriter>();
            // it might be scoped but its used in FileBackgroundService and it can't :(
            services.AddTransient<IRandomGenerator, RandomGenerator>();

            services.AddScoped<IFoaasClient, FoaasClient>();
            services.AddScoped<ICurrentUserService, CurrentUserService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseMiddleware<HeaderMiddleware>();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "action_constraints",
                    template: "{controller}/{action=Hi}/{name?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
