﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace MvcApp.TagHelpers
{
    [HtmlTargetElement(Attributes = "make-big")]
    public class MakeBigTagHelper : TagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.RemoveAll("MakeBig");
            output.Attributes.SetAttribute("style", "font-size: 50px");
        }
    }
}
