﻿using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace MvcApp.TagHelpers
{
    public class ButtonTagHelper : TagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.AddClass("btn", HtmlEncoder.Default);
            output.AddClass("btn-lg", HtmlEncoder.Default);

            output.RemoveClass("btn-default", HtmlEncoder.Default);
            output.RemoveClass("btn-danger", HtmlEncoder.Default);
            output.RemoveClass("btn-info", HtmlEncoder.Default);

            output.AddClass("btn-warning", HtmlEncoder.Default);
        }
    }
}
