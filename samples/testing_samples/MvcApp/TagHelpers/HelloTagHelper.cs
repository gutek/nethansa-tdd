﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace MvcApp.TagHelpers
{
    // it applies to hello tag with name attribute
    [HtmlTargetElement("hello", Attributes = nameof(Name))]
    public class HelloTagHelper : TagHelper
    {
        // conventions kicks in, so attribute name will be name
        // if we want different attribute name - use HtmlAttributeName attribute
        //[HtmlAttributeName("different")]
        public string Name { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "div";
            //output.Attributes.RemoveAll("name");
            output.Content.SetHtmlContent($"Hello, {Name}");
        }
    }
}
