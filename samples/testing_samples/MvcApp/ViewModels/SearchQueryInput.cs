﻿namespace MvcApp.ViewModels
{
    public class SearchQueryInput
    {
        public int Page { get; set; }
        public int Length { get; set; }
        public string Search { get; set; }
    }
}
