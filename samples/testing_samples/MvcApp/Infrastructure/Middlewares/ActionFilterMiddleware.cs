﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MvcApp.Services;

namespace MvcApp.Infrastructure.Middlewares
{
    public class ActionFilterMiddleware
    {
        private readonly RequestDelegate _next;

        public ActionFilterMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context
            , IFoaasClient client)
        {
            var result = await client.Awesome("middleware");
            var request = result.Message;

            context.Response.Headers.Add("X-Message", request);
            
            await _next(context);

            await context.Response.WriteAsync(request);
        }
    }
}