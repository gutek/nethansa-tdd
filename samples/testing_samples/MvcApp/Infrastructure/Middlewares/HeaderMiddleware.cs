﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MvcApp.Services;

namespace MvcApp.Infrastructure.Middlewares
{
    public class HeaderMiddleware
    {
        private readonly RequestDelegate _next;

        public HeaderMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context
            , IFoaasClient client)
        {
            if (context.Request.Path != "/hello")
            {
                await _next(context);
                return;
            }

            var header = "X-Message";
            string request = context.Request.Headers[header];

            if (string.IsNullOrEmpty(request))
            {
                var result = await client.Awesome("middleware");
                request = result.Message;
            }

            context.Response.Headers.Add(header, request);

            await context.Response.WriteAsync(request);

            await _next(context);
        }
    }
}
