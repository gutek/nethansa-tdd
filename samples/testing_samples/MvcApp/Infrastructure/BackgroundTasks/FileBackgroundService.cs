﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using MvcApp.Services;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace MvcApp.Infrastructure.BackgroundTasks
{
    public class FileBackgroundService : IHostedService, IDisposable
    {
        private readonly IHostingEnvironment _env;
        private readonly IFileWriter _writer;
        private readonly ITimerWrapper _timer;
        private readonly IRandomGenerator _random;
        private readonly int _procId;

        public FileBackgroundService(IHostingEnvironment env, IFileWriter writer, ITimerWrapper wrapper, IRandomGenerator random)
        {
            _env = env;
            _writer = writer;
            _timer = wrapper;
            _random = random;
            // this is out of scope, there is bug in xUnit and this is only done to satisfy this bug
            _procId = _random.Rand(int.MaxValue);
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            System.Diagnostics.Debug.WriteLine("+++++++++++++++++++Hosted service is staring.");
            
            _timer.Create(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(15));

            return Task.CompletedTask;
        }

        public void DoWork(object state)
        {
            Debug.WriteLine("-------------------------------Hosted service is DOING WORK.");
            
            _writer.Write(_env.WebRootPath, $"test_{_procId}.txt");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Debug.WriteLine("+++++++++++++++++++Hosted service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
