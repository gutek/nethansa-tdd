﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using MvcApp.Services;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace MvcApp.Infrastructure.BackgroundTasks
{
    public class FileBackgroundServiceInternalTimer : IHostedService, IDisposable
    {
        private readonly IHostingEnvironment _env;
        private readonly IFileWriter _writer;
        private Timer _timer;

        public FileBackgroundServiceInternalTimer(IHostingEnvironment env, IFileWriter writer)
        {
            _env = env;
            _writer = writer;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Debug.WriteLine("+++++++++++++++++++Hosted service is staring.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(15));

            return Task.CompletedTask;
        }

        // changing to public
        public void DoWork(object state)
        {
            Debug.WriteLine("-------------------------------Hosted service is DOING WORK.");

            _writer.Write(_env.WebRootPath, "test.txt");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Debug.WriteLine("+++++++++++++++++++Hosted service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}