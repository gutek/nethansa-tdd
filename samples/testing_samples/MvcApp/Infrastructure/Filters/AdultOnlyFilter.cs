﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using MvcApp.Services;

namespace MvcApp.Infrastructure.Filters
{
    public class AdultOnlyFilter : IAuthorizationFilter
    {
        private readonly ICurrentUserService _service;

        public AdultOnlyFilter(ICurrentUserService service)
        {
            _service = service;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = _service.Current();

            if (user.Age >= 18)
            {
                return;
            }

            // without message
            // var result1 = new BadRequestResult();
            // with message
            var result = new BadRequestObjectResult("You seem to young my fellow");
            context.Result = result;
        }
    }
}
