﻿using Microsoft.AspNetCore.Builder;
using MvcApp.Infrastructure.Middlewares;

namespace MvcApp.Infrastructure.Filters
{
    // as you can see, no tests at all. we only need to test
    // ActionFilterMiddleware no need to unit test UseMiddleware.
    public class ActionMiddlewareFilter
    {
        public void Configure(IApplicationBuilder builder)
        {
            builder.UseMiddleware<ActionFilterMiddleware>();
        }
    }
}