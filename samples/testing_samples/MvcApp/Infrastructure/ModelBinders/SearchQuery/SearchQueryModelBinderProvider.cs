﻿using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using MvcApp.Models;
using MvcApp.ViewModels;

namespace MvcApp.Infrastructure.ModelBinders.SearchQuery
{
    public class SearchQueryModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(SearchQueryInput))
            {
                return new BinderTypeModelBinder(typeof(SearchQueryModelBinder));
            }

            return null;
        }
    }
}
