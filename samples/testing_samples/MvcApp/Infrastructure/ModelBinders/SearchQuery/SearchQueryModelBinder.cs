﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MvcApp.ViewModels;

namespace MvcApp.Infrastructure.ModelBinders.SearchQuery
{
    public class SearchQueryModelBinder : IModelBinder
    {
        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var modelName = bindingContext.BinderModelName;
            if (string.IsNullOrEmpty(modelName))
            {
                modelName = "q";
            }

            var valueProviderResult = bindingContext
                .ValueProvider
                .GetValue(modelName);

            if (valueProviderResult == ValueProviderResult.None)
            {
                return;
            }

            var value = valueProviderResult.FirstValue;

            if (string.IsNullOrEmpty(value))
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return;
            }

            var split = value.Split(',');

            if (split.Length != 3)
            {

                bindingContext.Result = ModelBindingResult.Failed();
                return;
            }

            var page = split[0];
            var length = split[1];
            var search = split[2];

            var model = new SearchQueryInput();
            if (int.TryParse(page, out int pageVal))
            {
                model.Page = pageVal;
            }
            else
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return;
            }
            if (int.TryParse(length, out int lengthVal))
            {
                model.Length = lengthVal;
            }
            else
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return;
            }


            model.Search = search;
            bindingContext.Result = ModelBindingResult.Success(model);

            await Task.CompletedTask;
        }
    }
}
