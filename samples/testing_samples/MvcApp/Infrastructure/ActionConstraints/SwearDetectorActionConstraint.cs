﻿using System;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.Extensions.Primitives;

namespace MvcApp.Infrastructure.ActionConstraints
{
    public class SwearDetectorActionConstraint : Attribute
        , IActionConstraint
    {
        public string ParamName { get; set; }
        private const string _not_allowed = "butterfly leg";
        public bool Accept(ActionConstraintContext context)
        {
            // a way to check if we have param of name "name"
            //var nameParam =context.CurrentCandidate
            //    .Action
            //    .Parameters
            //    .FirstOrDefault(f => f.Name == "name");

            // check in route
            context.RouteContext
                .RouteData
                .Values
                .TryGetValue(ParamName, out object val);

            var str = val as string;
            if (str == null)
            {
                // check in query string
                if (context
                    .RouteContext
                    .HttpContext
                    .Request
                    .Query
                    .TryGetValue(ParamName, out StringValues strVal))
                {
                    str = strVal;
                }
                else
                {

                    return true;
                }
            }

            if (str.Contains(_not_allowed, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            return true;
        }

        public int Order => 1;
    }
}
