﻿using System;
using Microsoft.AspNetCore.Mvc.ActionConstraints;

namespace MvcApp.Infrastructure.ActionConstraints
{
    public class HeaderActionConstraint : Attribute, IActionConstraint
    {
        public bool Accept(ActionConstraintContext context)
        {
            var headers = context.RouteContext
                .HttpContext.Request.Headers;

            return headers.ContainsKey("X-Correlation-Id");
        }

        public int Order => 1;
    }
}
