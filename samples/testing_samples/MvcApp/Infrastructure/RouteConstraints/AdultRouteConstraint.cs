﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Routing.Constraints;

namespace MvcApp.Infrastructure.RouteConstraints
{
    public class AdultRouteConstraint : MinRouteConstraint
    {
        public AdultRouteConstraint() : base(18)
        {
        }
    }

    public class AdultRouteConstraint_CUSTOM_IMPL : IRouteConstraint
    {
        //private readonly IHostingEnvironment _environment;

        public AdultRouteConstraint_CUSTOM_IMPL(IHostingEnvironment environment)
        {
            Environment = environment;
        }
        /// <summary>
        /// Gets the minimum allowed value of the route parameter.
        /// </summary>
        public IHostingEnvironment Environment { get; }
        // no need
        public bool Match(HttpContext httpContext
            , IRouter route
            , string routeKey
            , RouteValueDictionary values
            , RouteDirection routeDirection)
        {
            //var match = base.Match(httpContext
            //    , route
            //    , routeKey
            //    , values
            //    , routeDirection);

            if (!values.TryGetValue(routeKey, out object obj))
            {
                return false;
            }

            if (obj is int)
            {
                return true;
            }

            return false;
        }
    }
}
