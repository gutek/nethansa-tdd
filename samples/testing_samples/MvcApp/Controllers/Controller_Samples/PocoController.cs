﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace MvcApp.Controllers
{
    /// <summary>
    /// This is just an example that we can do it and how to do it. I would not recommend doing this but
    /// there might be a situation that we will need it.
    ///
    /// BTW: don't use regions!
    /// </summary>
    public class PocoController
    {
        #region Option 1: Create View Data Manually in method

        public IActionResult IndexOption1()
        {
            // if we are not creating ViewData in constructor we can do it in
            // the method
            var viewData = new ViewDataDictionary<int>(
                new EmptyModelMetadataProvider()
                , new ModelStateDictionary()
            )
            {
                Model = 1
            };

            viewData.Add("Message", "Hello From POCO Controller!");

            var viewResult = new ViewResult
            {
                ViewName = "Index",
                ViewData = viewData
            };

            return viewResult;
        }

        #endregion Option 1: Create View Data Manually in method

        #region Option 2: Create View Data by action [FormService] injection
        public IActionResult IndexOption2([FromServices] IModelMetadataProvider provider)
        {
            // we can also inject Model Metadata provider into method
            var viewData = new ViewDataDictionary<int>(
                provider
                , new ModelStateDictionary()
            );

            viewData.Model = 2;
            viewData.Add("Message", "Hello From POCO Controller!");
            var viewResult = new ViewResult
            {
                ViewName = "Index",
                ViewData = viewData
            };

            return viewResult;
        }
        #endregion Option 2: Create View Data by action [FormService] injection

        #region Option 3: Create View Data in constructor by default injection

        // just so Option3 will not break build
        //public PocoController()
        //{

        //}

        // injection is not working any longer. we need to create it manually in constructor
        public ViewDataDictionary ViewData { get; set; }
        public PocoController(IModelMetadataProvider provider)
        {
            if (provider == null)
            {
                return;
            }

            ViewData = new ViewDataDictionary(
                provider
                , new ModelStateDictionary()
            );
        }

        public IActionResult IndexOption3()
        {
            // or use one created in constructor
            ViewData.Model = 3;
            ViewData.Add("Message", "Hello From POCO Controller!");

            var viewResult = new ViewResult
            {
                ViewName = "Index",
                ViewData = ViewData
            };

            return viewResult;
        }

        #endregion Option 3: Create View Data in constructor by default injection
    }
}