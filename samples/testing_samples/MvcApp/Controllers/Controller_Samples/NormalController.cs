﻿using Microsoft.AspNetCore.Mvc;

namespace MvcApp.Controllers
{
    public class NormalController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Message"] = "Hello From Normal Controller!";
            int model = 10;
            return View(model);
        }
    }
}