﻿using Microsoft.AspNetCore.Mvc;
using MvcApp.ViewModels;

namespace MvcApp.Controllers
{
    public class SearchController : Controller
    {
        // ... controller/action?q=1,10,search words
        public IActionResult Search(SearchQueryInput q)
        {
            // again, no tests for this simple controller action

            return View(q);
        }
    }
}