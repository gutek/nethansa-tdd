﻿using Microsoft.AspNetCore.Mvc;
using MvcApp.Infrastructure.Filters;

namespace MvcApp.Controllers
{
    public class AdultController : Controller
    {
        [Route("/adult/{age:adult}")]
        public IActionResult Content(int? age)
        {
            // as you can see. No test for route as we know that :adult will work (we have that tested)
            // also, we do not test action as there is nothing to test here.
            
            // but if we want we can do some simple test:
            // 1) if action param is passed to view
            // 2) if result of action is ViewResult with null name of View

            return View(age.Value);
        }

        [TypeFilter(typeof(AdultOnlyFilter))]
        public IActionResult Index()
        {
            return View();
        }

        [MiddlewareFilter(typeof(ActionMiddlewareFilter))]
        public IActionResult Middleware()
        {
            return View();
        }
    }
}