﻿using Microsoft.AspNetCore.Mvc;

namespace MvcApp.Controllers
{
    // honestly no need to unit test this sort of code!
    public class MotivateController : Controller
    {
        public IActionResult Me()
        {
            return View();
        }
    }
}