﻿using Microsoft.AspNetCore.Mvc;
using MvcApp.Infrastructure.ActionConstraints;

namespace MvcApp.Controllers
{
    public class ActionConstraintsController : Controller
    {
        // we have defined route in Startup with name? param, thats why
        // this code will work for route handler, otherwise route would
        // try to find param of name ID. name however would work for us
        // in query string.
        [SwearDetectorActionConstraint(ParamName = "name")]
        public IActionResult Hi(string name)
        {
            // yep, with string model we need to specify view name
            return View("Hi", name);
        }

        public IActionResult Hi()
        {
            // yep, with string model we need to specify view name
            return View("Hi", "BAD BOY!");
        }


        [HeaderActionConstraint]
        public IActionResult Header(string name)
        {
            return View("Header", name);
        }

        public IActionResult Header()
        {
            return View("Header", "Missing Header!");
        }
    }
}