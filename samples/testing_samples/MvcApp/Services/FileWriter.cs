﻿using System;
using System.IO;

namespace MvcApp.Services
{
    public interface IFileWriter
    {
        void Write(string basePath, string fileName);
    }

    /// <summary>
    /// This code can't be unit test as we are using AppendAllText. If we need
    /// to test that file naming or file content generator is doing what we
    /// want we could extract that from this method.
    /// </summary>
    public class FileWriter : IFileWriter
    {
        public void Write(string basePath, string fileName)
        {
            var path = basePath;
            var file = Path.Combine(path, fileName);

            var msg = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + Environment.NewLine;

            File.AppendAllText(file, msg);
        }
    }

}