﻿using System;
using MvcApp.Models;

namespace MvcApp.Services
{
    public interface IRandomGenerator
    {
        int Rand(int max = 36);
    }

    public class RandomGenerator : IRandomGenerator
    {
        public int Rand(int max = 36)
        {
            var rnd = new Random();
            return rnd.Next(max);
        }
    }

    public interface ICurrentUserService
    {
        User Current();
    }
    public class CurrentUserService : ICurrentUserService
    {
        private readonly IRandomGenerator _rnd;

        public CurrentUserService(IRandomGenerator rnd)
        {
            this._rnd = rnd;
        }

        public User Current()
        {
            var user = new User();

            user.Age = _rnd.Rand();
            user.FirstName = "Grzegorz";
            user.LastName = "Brzęczyszczykiewicz";

            return user;
        }
    }
}
