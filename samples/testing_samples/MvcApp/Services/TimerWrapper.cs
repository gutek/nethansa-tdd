﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MvcApp.Services
{
    public interface ITimerWrapper : IDisposable
    {
        bool Change(int dueTime, int period);
        void Create(TimerCallback callback, object state, TimeSpan dueTime, TimeSpan period);
    }

    public class TimerWrapper : ITimerWrapper
    {
        private Timer _timer;
        public void Dispose()
        {
            _timer?.Dispose();
        }

        public bool Change(int dueTime, int period)
        {
            _timer?.Change(dueTime, period);

            return true;
        }

        public void Create(TimerCallback callback, object state, TimeSpan dueTime, TimeSpan period)
        {
            _timer = new Timer(callback, state, dueTime, period);
        }
    }
}
