﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MvcApp.Services;

namespace MvcApp.ViewComponents
{
    // look under Views/Shared/Components/Motivate
    public class MotivateViewComponent : ViewComponent
    {
        private readonly IFoaasClient _client;
        public MotivateViewComponent(IFoaasClient client)
        {
            _client = client;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var response = await _client.Awesome("Neil");
            return View(response);
        }
    }
}
