﻿using System;
using Report.Data;
using SharedLib.ReadModel;

namespace SharedLib.Tests.read_model.report_data_read_model
{
    public class get_tests : IDisposable
    {
        private ReportDbContext _context;
        private ReportDataReadModel _readModel;

        public get_tests()
        {
            _context = new ReportDbContext();
            _readModel = new ReportDataReadModel(_context);
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

        /*
         * HERE WE SHOULD TEST DATA
         */
    }
}