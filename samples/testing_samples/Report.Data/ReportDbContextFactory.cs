﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Report.Data
{
    public class ReportDbContextFactory : IDesignTimeDbContextFactory<ReportDbContext>
    {
        public ReportDbContext CreateDbContext(string[] args)
        {
            // get this from startup
            var configuration = new ConfigurationBuilder().Build();

            var optionsBuilder = new DbContextOptionsBuilder<ReportDbContext>();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("UniverseDbContext"));


            return new ReportDbContext(optionsBuilder.Options);
        }
    }
}