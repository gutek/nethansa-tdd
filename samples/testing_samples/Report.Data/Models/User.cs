﻿using System;

namespace Report.Data.Models
{
    public class User
    {
        public Guid Id { get; set; }

        public string PhoneNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressZip { get; set; }
        public string AddressCity { get; set; }
    }
}