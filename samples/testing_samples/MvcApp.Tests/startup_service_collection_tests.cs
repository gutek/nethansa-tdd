﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MvcApp.Services;
using Xunit;

namespace MvcApp.Tests
{
    public class startup_service_collection_tests
    {
        private Startup _startup;

        public startup_service_collection_tests()
        {
            IConfiguration config = new ConfigurationBuilder().Build();
            _startup = new Startup(config);
        }

        /// <summary>
        /// it will throw and test will fail if not resolved
        /// </summary>
        [Fact]
        public void timewrapper_should_be_registered()
        {
            var service = new ServiceCollection();
            _startup.ConfigureServices(service);

            var provider = service.BuildServiceProvider();

            provider.GetService<ITimerWrapper>();
        }

        /// <summary>
        /// it will throw and test will fail if not resolved
        /// </summary>
        [Fact]
        public void foaas_should_be_registered()
        {
            var service = new ServiceCollection();
            _startup.ConfigureServices(service);

            var provider = service.BuildServiceProvider();

            provider.GetService<IFoaasClient>();
        }
    }
}