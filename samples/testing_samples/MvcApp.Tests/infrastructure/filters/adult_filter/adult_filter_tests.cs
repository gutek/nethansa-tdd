﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using MvcApp.Infrastructure.Filters;
using MvcApp.Services;
using NSubstitute;
using System.Linq;
using Xunit;

namespace MvcApp.Tests.infrastructure.filters.adult_filter
{
    public class adult_filter_tests
    {
        private IRandomGenerator _rnd;
        private CurrentUserService _service;
        private AdultOnlyFilter _filter;

        public adult_filter_tests()
        {
            _rnd = Substitute.For<IRandomGenerator>();
            // _service = Substitute.For<ICurrentUserService>();
            _service = new CurrentUserService(_rnd);
            _filter = new AdultOnlyFilter(_service);
        }

        private AuthorizationFilterContext get_auth_filter_context()
        {
            var httpCtx = new DefaultHttpContext();
            var actionDescriptor = new ActionDescriptor();
            var rd = new RouteData();
            var actionCtx = new ActionContext(httpCtx, rd, actionDescriptor);

            // nie zadziala, w pipeline jest weryfikacja czy nie ma nulli na http context itp
            // var actionCtx = new ActionContext();

            var filtersMeta = Enumerable.Empty<IFilterMetadata>().ToList();
            var ctx = new AuthorizationFilterContext(actionCtx, filtersMeta);

            return ctx;
        }

        [Fact]
        public void should_return_bad_request_object_result_if_user_is_not_adult()
        {
            // arrange
            _rnd.Rand().Returns(5);
            var ctx = get_auth_filter_context();

            // act
            _filter.OnAuthorization(ctx);

            // assert
            Assert.IsType<BadRequestObjectResult>(ctx.Result);
        }
        [Fact]
        public void should_not_not_set_result_for_adults()
        {
            // arrange
            _rnd.Rand().Returns(19);
            var ctx = get_auth_filter_context();

            // act
            _filter.OnAuthorization(ctx);

            // assert
            Assert.Null(ctx.Result);
        }
    }
}
