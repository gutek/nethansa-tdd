﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.AspNetCore.Routing;
using MvcApp.Infrastructure.ActionConstraints;
using Xunit;

namespace MvcApp.Tests.infrastructure.action_constraints.swear_detector_action
{
    public class swear_detector_action_constraints_tests
    {
        private SwearDetectorActionConstraint _constraint;

        public swear_detector_action_constraints_tests()
        {
            _constraint = new SwearDetectorActionConstraint();
            _constraint.ParamName = "name";
        }

        private ActionConstraintContext prepare_context(HttpContext context
            , bool set_name = false
            , string name = "Tomek")
        {
            var ctx = new ActionConstraintContext();

            ctx.RouteContext = new RouteContext(context);
            var rd = new RouteData();
            if (set_name)
            {
                rd.Values.Add("name", name);
            }
            ctx.RouteContext.RouteData = rd;

            return ctx;
        }

        private HttpContext prepare_http_context(bool set_name = false
            , string name = "Tomek")
        {
            var ctx = new DefaultHttpContext();

            if (set_name)
            {
                ctx.Request.QueryString = new QueryString($"?name={name}");
            }

            return ctx;
        }

        [Fact]
        public void should_return_true_if_name_does_not_exists_in_query_string_nor_in_route_data()
        {
            var context = prepare_http_context();
            var ctx = prepare_context(context);

            var result = _constraint.Accept(ctx);

            Assert.True(result);
        }
        [Fact]
        public void should_return_true_if_name_does_exists_in_route_data_and_its_not_motyla_noga_and_query_string_does_not_exsist()
        {
            var context = prepare_http_context();
            var ctx = prepare_context(context, true);

            var result = _constraint.Accept(ctx);

            Assert.True(result);
        }
        [Fact]
        public void should_return_true_if_name_does_exists_in_query_string_and_its_not_motyla_noga_and_route_data_does_not_exsist()
        {
            var context = prepare_http_context(true);
            var ctx = prepare_context(context);

            var result = _constraint.Accept(ctx);

            Assert.True(result);
        }
        [Fact]
        public void should_return_true_if_name_does_exists_in_query_string_and_in_route_data_does_and_its_not_motyla_noga()
        {
            var context = prepare_http_context(true);
            var ctx = prepare_context(context, true);

            var result = _constraint.Accept(ctx);

            Assert.True(result);
        }
        [Fact]
        public void should_retrn_false_if_motyla_noga_exists_in_route_data()
        {
            var context = prepare_http_context(false);
            var ctx = prepare_context(context, true, name: "butterfly leg");

            var result = _constraint.Accept(ctx);

            Assert.False(result);
        }
        [Fact]
        public void should_return_false_if_motyla_noga_exists_in_query_string()
        {
            var context = prepare_http_context(true, name: "butterfly leg");
            var ctx = prepare_context(context, false);

            var result = _constraint.Accept(ctx);

            Assert.False(result);
        }
        [Fact]
        public void should_retrn_false_if_motyla_noga_exists_in_both()
        {
            var context = prepare_http_context(true, name: "butterfly leg");
            var ctx = prepare_context(context, true, name: "butterfly leg");

            var result = _constraint.Accept(ctx);

            Assert.False(result);
        }
    }
}
