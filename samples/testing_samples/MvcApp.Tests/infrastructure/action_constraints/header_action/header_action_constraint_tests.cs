﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.AspNetCore.Routing;
using MvcApp.Infrastructure.ActionConstraints;
using Xunit;

namespace MvcApp.Tests.infrastructure.action_constraints.header_action
{
    public class header_action_constraint_tests
    {
        private HeaderActionConstraint _constraint;

        public header_action_constraint_tests()
        {
            _constraint = new HeaderActionConstraint();
        }

        [Fact]
        public void should_allow_if_correlation_header_is_set()
        {
            var ctx = new ActionConstraintContext();
            var context = new DefaultHttpContext();

            context.Request.Headers.Add("X-Correlation-Id", "SOME");

            ctx.RouteContext = new RouteContext(context);

            var result = _constraint.Accept(ctx);

            Assert.True(result);
        }
        [Fact]
        public void should_not_allow_if_correlation_header_is_not_set()
        {
            var ctx = new ActionConstraintContext();
            var context = new DefaultHttpContext();
            ctx.RouteContext = new RouteContext(context);

            var result = _constraint.Accept(ctx);

            Assert.False(result);
        }
    }
}
