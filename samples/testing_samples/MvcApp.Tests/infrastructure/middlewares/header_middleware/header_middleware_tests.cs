﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MvcApp.Infrastructure.Middlewares;
using MvcApp.Models;
using MvcApp.Services;
using NSubstitute;
using Xunit;

namespace MvcApp.Tests.infrastructure.middlewares.header_middleware
{
    public class action_filter_middleware_tests
    {
        private HeaderMiddleware _middleware;
        private bool _delegateCalled;

        public action_filter_middleware_tests()
        {
            _middleware = new HeaderMiddleware(ctx =>
            {
                _delegateCalled = true;
                return Task.CompletedTask;
            });
        }

        [Fact]
        public async Task
            should_call_external_service_if_path_is_hello_but_header_x_message_has_not_been_set_on_request()
        {
            var path = "/hello";
            var result = new Response
            {
                Message = "msg",
                Subtitle = "sbt"
            };
            var context = prepare_context();
            var foaas = prepare_client(result);

            context.Request.Path = path;

            await _middleware.Invoke(context, foaas);

            await foaas.Received().Awesome(Arg.Any<string>());
        }
        [Fact]
        public async Task
            should_not_call_external_service_if_path_is_hello_and_header_x_message_has_been_set_on_request()
        {
            var path = "/hello";
            var result = new Response
            {
                Message = "msg",
                Subtitle = "sbt"
            };
            var context = prepare_context();
            var foaas = prepare_client(result);

            context.Request.Path = path;
            context.Request.Headers.Add("X-Message", "test");

            await _middleware.Invoke(context, foaas);

            await foaas.DidNotReceive().Awesome(Arg.Any<string>());
        }

        [Fact]
        public async Task should_not_set_header_for_path_different_than_hello()
        {
            var path = "/test";

            var context = Substitute.For<HttpContext>();
            var foaas = Substitute.For<IFoaasClient>();

            context.Request.Path = path;

            // act
            await _middleware.Invoke(context, foaas);

            var exists = context.Response.Headers.TryGetValue("X-Message", out _);

            // assert
            Assert.False(exists);
        }

        [Fact]
        public async Task should_set_header_for_path_hello()
        {
            var path = "/hello";
            var result = new Response
            {
                Message = "msg",
                Subtitle = "sbt"
            };
            var context = prepare_context();
            var foaas = prepare_client(result);

            context.Request.Path = path;

            // act
            await _middleware.Invoke(context, foaas);

            var exists = context.Response.Headers.TryGetValue("X-Message", out _);
            // assert
            Assert.True(exists);
        }

        [Fact]
        public async Task should_set_body()
        {
            var path = "/hello";
            var result = new Response
            {
                Message = "msg",
                Subtitle = "sbt"
            };

            var context = prepare_context();
            var foaas = prepare_client(result);

            context.Request.Path = path;

            // act
            await _middleware.Invoke(context, foaas);

            context.Response.Body.Seek(0, SeekOrigin.Begin);
            using (var reader = new StreamReader(context.Response.Body))
            {
                var streamText = reader.ReadToEnd();

                // assert
                Assert.False(string.IsNullOrEmpty(streamText));
                Assert.Equal("msg", streamText);
            }
        }

        private HttpContext prepare_context()
        {

            //var context = Substitute.For<HttpContext>();
            //context.Response.Body.Returns(new MemoryStream());
            //context.Response.Headers.Returns(new HeaderDictionary());

            var context = new DefaultHttpContext();
            context.Response.Body = new MemoryStream();

            return context;
        }

        private IFoaasClient prepare_client(Response response)
        {
            var foaas = Substitute.For<IFoaasClient>();
            foaas.Awesome(Arg.Any<string>()).Returns(response);
            return foaas;
        }
    }
}
