﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MvcApp.Infrastructure.Middlewares;
using MvcApp.Models;
using MvcApp.Services;
using NSubstitute;
using Xunit;

namespace MvcApp.Tests.infrastructure.middlewares.action_filter_middleware
{
    public class action_filter_middleware_tests
    {
        private ActionFilterMiddleware _middleware;
        private bool _delegateCalled;

        public action_filter_middleware_tests()
        {
            _middleware = new ActionFilterMiddleware(ctx =>
            {
                _delegateCalled = true;
                return Task.CompletedTask;
            });
        }

        [Theory]
        [InlineData("/hello")]
        [InlineData("/complex/route")]
        [InlineData("/route/with/query/string?str=10")]
        public async Task should_set_header_for_all_paths(string path)
        {
            var result = new Response
            {
                Message = "msg",
                Subtitle = "sbt"
            };
            var context = prepare_context();
            var foaas = prepare_client(result);

            context.Request.Path = path;

            // act
            await _middleware.Invoke(context, foaas);

            var exists = context.Response.Headers.TryGetValue("X-Message", out _);
            // assert
            Assert.True(exists);
        }

        [Theory]
        [InlineData("/hello")]
        [InlineData("/complex/route")]
        [InlineData("/route/with/query/string?str=10")]
        public async Task should_set_body(string path)
        {
            var result = new Response
            {
                Message = "msg",
                Subtitle = "sbt"
            };

            var context = prepare_context();
            var foaas = prepare_client(result);

            context.Request.Path = path;

            // act
            await _middleware.Invoke(context, foaas);

            context.Response.Body.Seek(0, SeekOrigin.Begin);
            using (var reader = new StreamReader(context.Response.Body))
            {
                var streamText = reader.ReadToEnd();

                // assert
                Assert.False(string.IsNullOrEmpty(streamText));
                Assert.Equal("msg", streamText);
            }
        }

        private HttpContext prepare_context()
        {

            //var context = Substitute.For<HttpContext>();
            //context.Response.Body.Returns(new MemoryStream());
            //context.Response.Headers.Returns(new HeaderDictionary());

            var context = new DefaultHttpContext();
            context.Response.Body = new MemoryStream();

            return context;
        }

        private IFoaasClient prepare_client(Response response)
        {
            var foaas = Substitute.For<IFoaasClient>();
            foaas.Awesome(Arg.Any<string>()).Returns(response);
            return foaas;
        }
    }
}
