﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using MvcApp.Infrastructure.ModelBinders.SearchQuery;
using MvcApp.Tests.Helpers;
using MvcApp.ViewModels;
using System;
using System.Threading.Tasks;
using Xunit;

namespace MvcApp.Tests.infrastructure.model_binders.search_query
{
    public class search_query_model_binder_tests
    {
        private SearchQueryModelBinder _binder;

        public search_query_model_binder_tests()
        {
            _binder = new SearchQueryModelBinder();
        }

        [Theory]
        [InlineData("1,2,aaa", 1, 2, "aaa")]
        public async Task should_bind_properly(string query
            , int page
            , int length
            , string search)
        {
            var ctx = GetBinderAndContext(typeof(SearchQueryInput), query);

            await _binder.BindModelAsync(ctx.Item1);

            Assert.True(ctx.Item1.Result.IsModelSet);
            //Assert.Null(ctx.Item1.Result.Model);
            Assert.True(ctx.Item1.ModelState.IsValid);

            Assert.IsType<SearchQueryInput>(ctx.Item1.Result.Model);
            var model = ctx.Item1.Result.Model as SearchQueryInput;

            Assert.Equal(page, model.Page);
            Assert.Equal(length, model.Length);
            Assert.Equal(search, model.Search);

        }

        private static (DefaultModelBindingContext, IModelBinder) GetBinderAndContext(
            Type modelType,
            object valueProviderValue)
        {
            var binderProviderContext = new TestModelBinderProviderContext(modelType);
            var modelName = "q";
            var bindingContext = new DefaultModelBindingContext
            {
                ModelMetadata = binderProviderContext.Metadata,
                ModelName = modelName,
                ModelState = new ModelStateDictionary(),
                ValueProvider = new SimpleValueProvider
                {
                    { modelName, valueProviderValue }
                }
            };
            var binderProvider = new SearchQueryModelBinderProvider();
            var binder = binderProvider.GetBinder(binderProviderContext);
            return (bindingContext, binder);
        }
    }
}
