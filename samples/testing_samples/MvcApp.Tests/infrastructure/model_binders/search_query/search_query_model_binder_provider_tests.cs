﻿using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using MvcApp.Infrastructure.ModelBinders.SearchQuery;
using MvcApp.Tests.Helpers;
using MvcApp.ViewModels;
using Xunit;

namespace MvcApp.Tests.infrastructure.model_binders.search_query
{
    public class search_query_model_binder_provider_tests
    {
        private SearchQueryModelBinderProvider _provider;

        public search_query_model_binder_provider_tests()
        {
            _provider = new SearchQueryModelBinderProvider();
        }

        [Fact]
        public void should_throw_when_context_is_null()
        {
            Assert.Throws<ArgumentNullException>(() => _provider.GetBinder(null));
        }

        [Fact]
        public void should_return_null_if_type_is_unrecognized()
        {
            var ctx = new TestModelBinderProviderContext(typeof(object));

            var result = _provider.GetBinder(ctx);

            Assert.Null(result);

        }
        [Fact]
        public void should_return_BinderTypeModelBinder_if_type_is_recognized()
        {
            var ctx = new TestModelBinderProviderContext(typeof(SearchQueryInput));

            var result = _provider.GetBinder(ctx);

            Assert.IsType<BinderTypeModelBinder>(result);
        }
        [Fact(Skip = "we can't test that - we would need to return our Binder instead of factory binder")]
        public void BinderTypeModelBinder_should_initialize_SearchQueryModelBinder()
        {
            var ctx = new TestModelBinderProviderContext(typeof(SearchQueryInput));

            var result = _provider.GetBinder(ctx);

            Assert.IsType<SearchQueryModelBinder>(result);
        }
    }
}
