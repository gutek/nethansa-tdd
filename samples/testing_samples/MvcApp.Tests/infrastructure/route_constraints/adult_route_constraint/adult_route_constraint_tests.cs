﻿using MvcApp.Infrastructure.RouteConstraints;
using MvcApp.Tests.Helpers;
using Xunit;

namespace MvcApp.Tests.infrastructure.route_constraints.adult_route_constraint
{
    public class adult_route_constraint_tests
    {
        private AdultRouteConstraint _constraint;

        public adult_route_constraint_tests()
        {
            _constraint = new AdultRouteConstraint();
        }

        [Theory]
        [InlineData(18)]
        [InlineData(20)]
        [InlineData(500)]
        public void should_match_only_age_above_or_equal_to_18(int age)
        {
            var result = ConstraintsTestHelper.TestConstraint(_constraint, age);

            Assert.True(result);
        }

        [Theory]
        [InlineData(17)]
        [InlineData(1)]
        [InlineData(0)]
        public void should_not_match_for_age_below_18(int age)
        {
            var result = ConstraintsTestHelper.TestConstraint(_constraint, age);

            Assert.False(result);
        }

        [Theory]
        [InlineData(15.1)]
        [InlineData(20.6)]
        public void should_not_match_values_different_than_int(object age)
        {
            var result = ConstraintsTestHelper.TestConstraint(_constraint, age);

            Assert.False(result);
        }
    }
}
