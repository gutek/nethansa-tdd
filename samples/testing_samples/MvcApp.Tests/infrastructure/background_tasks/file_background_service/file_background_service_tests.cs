﻿using Microsoft.AspNetCore.Hosting;
using NSubstitute;
using MvcApp.Infrastructure.BackgroundTasks;
using MvcApp.Services;
using Xunit;
using System.Threading;
using System;

namespace MvcApp.Tests.infrastructure.background_tasks.file_background_service_internal_timer
{
    public class file_background_service_tests
    {
        private IFileWriter _fileWriter;
        private IHostingEnvironment _hosting;
        private FileBackgroundService _background;
        private ITimerWrapper _timer;
        private IRandomGenerator _random;

        public file_background_service_tests()
        {
            _fileWriter = Substitute.For<IFileWriter>();
            // be careful what HostingEnvironment you are using!!!!!!
            _hosting = Substitute.For<IHostingEnvironment>();
            _timer = Substitute.For<ITimerWrapper>();
            _random = Substitute.For<IRandomGenerator>();
            _background = new FileBackgroundService(_hosting, _fileWriter, _timer, _random);
        }

        [Fact]
        public void should_create_timer()
        {
            _background.StartAsync(CancellationToken.None);

            _timer
                .Received(1)
                .Create(_background.DoWork, null, Arg.Any<TimeSpan>(), Arg.Any<TimeSpan>());
        }

        [Fact]
        public void should_change_timer_on_stop()
        {
            _background.StopAsync(CancellationToken.None);

            _timer
                .Received(1)
                .Change(Arg.Any<int>(), Arg.Any<int>());
        }
        [Fact]
        public void timer_should_be_disposed()
        {
            _background.Dispose();

            _timer
                .Received(1)
                .Dispose();
        }

        [Fact]
        public void should_call_write()
        {
            _background.DoWork(null);

            _fileWriter
                .Received(1)
                .Write(Arg.Any<string>(), Arg.Any<string>());
        }
        [Fact]
        public void should_call_www_root()
        {
            _background.DoWork(null);

            var get = _hosting.Received(1).WebRootPath;
        }
    }
}
