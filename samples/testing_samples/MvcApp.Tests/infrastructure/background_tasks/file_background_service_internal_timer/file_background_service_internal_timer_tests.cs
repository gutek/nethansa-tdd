﻿using Microsoft.AspNetCore.Hosting;
using NSubstitute;
using MvcApp.Infrastructure.BackgroundTasks;
using MvcApp.Services;
using Xunit;

namespace MvcApp.Tests.infrastructure.background_tasks.file_background_service_internal_timer
{
    public class file_background_service_internal_timer_tests
    {
        private IFileWriter _fileWriter;
        private IHostingEnvironment _hosting;
        private FileBackgroundServiceInternalTimer _background;

        public file_background_service_internal_timer_tests()
        {
            _fileWriter = Substitute.For<IFileWriter>();
            // uwaga KTORY HostingEnvironment!!!!!!
            _hosting = Substitute.For<IHostingEnvironment>();
            _background = new FileBackgroundServiceInternalTimer(_hosting, _fileWriter);
        }
        [Fact]
        public void should_call_write()
        {
            _background.DoWork(null);

            _fileWriter
                .Received(1)
                .Write(Arg.Any<string>(), Arg.Any<string>());
        }
        [Fact]
        public void should_call_www_root()
        {
            _background.DoWork(null);

            var get = _hosting.Received(1).WebRootPath;
        }
    }
}
