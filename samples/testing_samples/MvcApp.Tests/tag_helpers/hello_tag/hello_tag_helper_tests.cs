﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Razor.TagHelpers;
using MvcApp.TagHelpers;
using Xunit;

namespace MvcApp.Tests.tag_helpers.button_tag
{
    public class hello_tag_helper_tests
    {
        private HelloTagHelper _tag;

        public hello_tag_helper_tests()
        {
            _tag = new HelloTagHelper();
        }

        [Fact]
        public void should_return_div()
        {
            // to co jest na wejsciu
            var ctx = MakeTagHelperContextSlim();

            var output = MakeTagHelperOutputSlim();

            _tag.Process(ctx, output);

            Assert.Equal("div", output.TagName);
        }
        [Fact]
        public void should_say_hello()
        {
            // wea re not using attributes so we don't need them
            //var ctx = MakeTagHelperContext(new TagHelperAttributeList(new[]
            //{
            //    new TagHelperAttribute("name", "tomek")
            //}));

            var ctx = MakeTagHelperContextSlim();
            var output = MakeTagHelperOutputSlim();

            _tag.Name = "tomek";

            _tag.Process(ctx, output);

            var content = output.Content.GetContent();

            Assert.Equal("Hello, tomek", content);
        }

        private TagHelperContext MakeTagHelperContext(TagHelperAttributeList attributes)
        {
            return new TagHelperContext(
                tagName: "home",
                allAttributes: attributes,
                items: new Dictionary<object, object>(),
                uniqueId: Guid.NewGuid().ToString("N"));
        }

        private static TagHelperOutput MakeTagHelperOutput(TagHelperAttributeList attributes)
        {
            attributes = attributes ?? new TagHelperAttributeList();

            var dthc = new DefaultTagHelperContent();
            return new TagHelperOutput(
                tagName: null,
                attributes: attributes,
                getChildContentAsync: (useCachedResult, encoder) => Task.FromResult<TagHelperContent>(dthc));
        }
        private TagHelperContext MakeTagHelperContextSlim()
        {
            return new TagHelperContext(
                tagName: "home",
                allAttributes: new TagHelperAttributeList(),
                items: new Dictionary<object, object>(),
                uniqueId: Guid.NewGuid().ToString("N"));
        }

        private static TagHelperOutput MakeTagHelperOutputSlim()
        {
            var dthc = new DefaultTagHelperContent();
            return new TagHelperOutput(
                tagName: null,
                attributes: new TagHelperAttributeList(),
                getChildContentAsync: (useCachedResult, encoder) => Task.FromResult<TagHelperContent>(dthc));
        }
    }
}
