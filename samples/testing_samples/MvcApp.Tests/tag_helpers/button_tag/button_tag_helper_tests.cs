﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Razor.TagHelpers;
using MvcApp.TagHelpers;
using Xunit;

namespace MvcApp.Tests.tag_helpers.hello_tag
{
    public class button_tag_helper_tests
    {
        private ButtonTagHelper _tag;

        public button_tag_helper_tests()
        {
            _tag = new ButtonTagHelper();
        }

        [Fact]
        public void should_add_btn_warning_class()
        {
            // what we have on entrence
            var ctx = MakeTagHelperContext(null);

            var output = MakeTagHelperOutput(null);

            _tag.Process(ctx, output);

            // we should have class here
            var btnClass = output.Attributes["class"].Value as HtmlString;

            Assert.Contains("btn-warning", btnClass.Value);
        }
        [Fact]
        public void should_not_copy_make_big_attribute()
        {
            // what we have on entrence
            var ctx = MakeTagHelperContext(null, tagName: "div");

            var output = MakeTagHelperOutput(null);

            _tag.Process(ctx, output);

            Assert.False(output.Attributes.ContainsName("make-big"));
        }

        private TagHelperContext MakeTagHelperContext(TagHelperAttributeList attributes, string tagName = "div")
        {
            attributes = attributes ?? new TagHelperAttributeList();
            return new TagHelperContext(
                tagName: tagName,
                allAttributes: attributes,
                items: new Dictionary<object, object>(),
                uniqueId: Guid.NewGuid().ToString("N"));
        }

        private static TagHelperOutput MakeTagHelperOutput(TagHelperAttributeList attributes)
        {
            attributes = attributes ?? new TagHelperAttributeList();

            var dthc = new DefaultTagHelperContent();
            return new TagHelperOutput(
                tagName: null,
                attributes: attributes,
                getChildContentAsync: (useCachedResult, encoder) => Task.FromResult<TagHelperContent>(dthc));
        }
        private TagHelperContext MakeTagHelperContextSlim()
        {
            return new TagHelperContext(
                tagName: "home",
                allAttributes: new TagHelperAttributeList(),
                items: new Dictionary<object, object>(),
                uniqueId: Guid.NewGuid().ToString("N"));
        }

        private static TagHelperOutput MakeTagHelperOutputSlim()
        {
            var dthc = new DefaultTagHelperContent();
            return new TagHelperOutput(
                tagName: null,
                attributes: new TagHelperAttributeList(),
                getChildContentAsync: (useCachedResult, encoder) => Task.FromResult<TagHelperContent>(dthc));
        }
    }
}
