﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Razor.TagHelpers;
using MvcApp.TagHelpers;
using Xunit;

namespace MvcApp.Tests.tag_helpers.make_big_attribute
{
    public class makebig_tag_helper_tests
    {
        private MakeBigTagHelper _tag;

        public makebig_tag_helper_tests()
        {
            _tag = new MakeBigTagHelper();
        }

        [Fact]
        public void should_not_change_input_element_name()
        {
            // what we have on entrence
            var ctx = MakeTagHelperContext(null, tagName: "div");

            var output = MakeTagHelperOutput(null);

            _tag.Process(ctx, output);

            Assert.Null(output.TagName);
        }
        [Fact]
        public void should_not_copy_make_big_attribute()
        {
            // what we have on entrence
            var ctx = MakeTagHelperContext(null, tagName: "div");

            var output = MakeTagHelperOutput(null);

            _tag.Process(ctx, output);

            Assert.False(output.Attributes.ContainsName("make-big"));
        }
        [Fact]
        public void should_create_style_attribute()
        {
            // what we have on entrence
            var ctx = MakeTagHelperContext(null, tagName: "div");

            var output = MakeTagHelperOutput(null);

            _tag.Process(ctx, output);

            Assert.True(output.Attributes.ContainsName("style"));
        }


        private TagHelperContext MakeTagHelperContext(TagHelperAttributeList attributes, string tagName = "div")
        {
            attributes = attributes ?? new TagHelperAttributeList();
            return new TagHelperContext(
                tagName: tagName,
                allAttributes: attributes,
                items: new Dictionary<object, object>(),
                uniqueId: Guid.NewGuid().ToString("N"));
        }

        private static TagHelperOutput MakeTagHelperOutput(TagHelperAttributeList attributes)
        {
            attributes = attributes ?? new TagHelperAttributeList();

            var dthc = new DefaultTagHelperContent();
            return new TagHelperOutput(
                tagName: null,
                attributes: attributes,
                getChildContentAsync: (useCachedResult, encoder) => Task.FromResult<TagHelperContent>(dthc));
        }
        private TagHelperContext MakeTagHelperContextSlim()
        {
            return new TagHelperContext(
                tagName: "home",
                allAttributes: new TagHelperAttributeList(),
                items: new Dictionary<object, object>(),
                uniqueId: Guid.NewGuid().ToString("N"));
        }

        private static TagHelperOutput MakeTagHelperOutputSlim()
        {
            var dthc = new DefaultTagHelperContent();
            return new TagHelperOutput(
                tagName: null,
                attributes: new TagHelperAttributeList(),
                getChildContentAsync: (useCachedResult, encoder) => Task.FromResult<TagHelperContent>(dthc));
        }
    }
}
