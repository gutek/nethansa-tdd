﻿// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// https://github.com/aspnet/Routing/blob/2.1.0/test/Microsoft.AspNetCore.Routing.Tests/Constraints/ConstraintsTestHelper.cs

using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace MvcApp.Tests.Helpers
{
    public class ConstraintsTestHelper
    {
        public static bool TestConstraint(IRouteConstraint constraint, object value, Action<IRouter> routeConfig = null)
        {
            var context = new DefaultHttpContext();

            var route = new RouteCollection();

            routeConfig?.Invoke(route);

            var parameterName = "fake";
            var values = new RouteValueDictionary
            {
                { parameterName, value }
            };

            var routeDirection = RouteDirection.IncomingRequest;

            var match = constraint.Match(context, route, parameterName, values, routeDirection);

            return match;
        }
    }
}
