﻿using Microsoft.AspNetCore.Mvc;
using MvcApp.Controllers;
using Xunit;

namespace MvcApp.Tests.controllers.controller_samples.poco
{
    public class poco_controller_option1_tests
    {
        private PocoController _controller;

        public poco_controller_option1_tests()
        {
            _controller = new PocoController(null);
        }

        [Fact]
        public void should_set_message_in_view_data()
        {
            // act
            var result = _controller.IndexOption1() as ViewResult;

            var msg = result.ViewData.TryGetValue("Message", out object value);

            Assert.True(msg);
        }

        [Fact]
        public void message_should_be_set_to_hello()
        {
            var expected = "Hello From POCO Controller!";

            // act
            var result = _controller.IndexOption1() as ViewResult;

            var actual = result.ViewData["Message"] as string;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void result_should_be_of_ViewResult_type()
        {
            var viewName = "Index";
            // act
            var result = _controller.IndexOption1();
            var view = result as ViewResult;

            Assert.IsType<ViewResult>(result);
            Assert.NotNull(view);
            Assert.Equal(viewName, view.ViewName);
        }
    }
}
