﻿using Microsoft.AspNetCore.Mvc;
using MvcApp.Controllers;
using Xunit;

namespace MvcApp.Tests.controllers.controller_samples.normal
{
    public class normal_controller_tests
    {
        private NormalController _controller;

        public normal_controller_tests()
        {
            _controller = new NormalController();
        }
        [Fact]
        public void should_set_message_in_view_data()
        {
            // act
            var result = _controller.Index() as ViewResult;

            var msg = result.ViewData.TryGetValue("Message", out object value);

            Assert.True(msg);
        }
        [Fact]
        public void result_should_be_of_ViewResult_type()
        {
            // act
            var result = _controller.Index();

            Assert.IsType<ViewResult>(result);
        }
        [Fact]
        public void message_should_be_set_to_hello()
        {
            var expected = "Hello From Normal Controller!";

            // act
            var result = _controller.Index() as ViewResult;

            var actual = result.ViewData["Message"] as string;

            Assert.Equal(expected, actual);
        }
    }
}
