﻿using MvcApp.Services;
using NSubstitute;
using Xunit;
using MvcApp.Models;
using NSubstitute.ReceivedExtensions;

namespace MvcApp.Tests.services.current_user_service
{
    public class current_user_service_tests
    {
        private IRandomGenerator _random;
        private CurrentUserService _service;

        public current_user_service_tests()
        {
            _random = Substitute.For<IRandomGenerator>();
            _service = new CurrentUserService(_random);
        }

        [Fact]
        public void should_return_user_object()
        {
            var user = _service.Current();

            Assert.NotNull(user);
            Assert.IsType<User>(user);
        }

        [Fact]
        public void should_execute_random_generator()
        {
            var user = _service.Current();

            _random.Received().Rand();
        }
        [Fact]
        public void should_assign_value_from_random()
        {
            var ten = 10;
            _random.Rand().Returns(ten);

            var user = _service.Current();

            Assert.Equal(ten, user.Age);
        }
    }
}