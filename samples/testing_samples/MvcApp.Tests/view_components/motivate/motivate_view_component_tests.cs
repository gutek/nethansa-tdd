﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using NSubstitute;
using MvcApp.Services;
using MvcApp.ViewComponents;
using Xunit;

namespace MvcApp.Tests.view_components.motivate
{
    public class motivate_view_component_tests
    {
        private IFoaasClient _service;
        private MotivateViewComponent _vc;

        public motivate_view_component_tests()
        {
            _service = Substitute.For<IFoaasClient>();
            _vc = new MotivateViewComponent(_service);
        }
        [Fact]
        public async Task should_return_view_component_result()
        {
            var result = await _vc.InvokeAsync();

            Assert.IsType<ViewViewComponentResult>(result);
        }
        [Fact]
        public async Task should_return_default_view()
        {
            var result = (await _vc.InvokeAsync()) as ViewViewComponentResult;

            Assert.Null(result.ViewName);
        }

        [Fact]
        public async Task should_call_foaas()
        {
            await _vc.InvokeAsync();

            await _service.Received(1).Awesome(Arg.Any<string>());
        }
    }
}
