﻿using System;

namespace BlackboxSystem
{
    public class SayHello
    {
        public string Hi(string name, int age)
        {
            if (age > 81)
            {
                throw new ArgumentException("too old", nameof(age));
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("you're ghost", nameof(name));
            }

            var day = DateTime.Now.Day;

            if (day % 2 == 0)
            {
                return $"Hi, {name}, today is: {day} and your are {age - day} years old";
            }

            return $"Hi, {name}, today is: {day} and your are {age} yearls old";
        }
    }
}