﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Threading;

namespace BlackboxSystem
{
    public class CalculatePrime
    {
        public bool IsPrime(int number)
        {
            var rnd = new Random();
            var num = rnd.Next(0, int.MaxValue);

            if (num > number && num % number == 0)
            {
                return true;
            }

            if (num <= number && number % num == 0)
            {
                return true;
            }

            Thread.Sleep(1000);
            var url = $"http://numbers.mezurashico.com/primes/{number}";

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                {
                    var ser = new DataContractJsonSerializer(typeof(PrimeObj));

                    var prime = ser.ReadObject(stream) as PrimeObj;

                    return prime.prime;
                }
            }
            catch (WebException ex)
            {
                // if is not prime, 404 is returned
                return false;
            }
        }
    }
}