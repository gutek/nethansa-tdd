﻿using System;

namespace BlackboxSystem
{
    public class Calculator
    {
        public int Add(int a, int b)
        {
            if (b < 0)
            {
                b = Math.Abs(b);
            }

            if (!will_overflow(a, b))
            {
                return a + b;
            }

            throw new Exception("OVERFLOW");
        }

        private static bool will_overflow(int a, int b)
        {
            return int.MaxValue - a < b;
        }
    }
}
