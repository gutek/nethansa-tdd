﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Parser.Html;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;

namespace Web.Tests.magic
{
    public class angler_sample_tests
    {
        // potrzebny bo cos musi hostowac nasza apke!!!
        private TestServer _server;
        private HttpClient _client;

        public angler_sample_tests()
        {
            _server = new TestServer(new WebHostBuilder()
                .UseContentRoot(@"D:\workshops\aspnet-core-base\demos\29\Web")
                .UseEnvironment("Development")
                .UseStartup<Startup>()
            );

            _client = _server.CreateClient();
        }
        
        private async Task<string> execute(string path)
        {
            var result = await _client.GetAsync(path);
            var content = await result.Content.ReadAsStringAsync();

            return content;
        }
        
        [Fact]
        public async Task should_return_span()
        {
            var result = await execute("/home/index");
            var parser = new HtmlParser();
            var parsed = await parser.ParseAsync(result);

            var spans = parsed.QuerySelectorAll("span");

            Assert.NotEmpty(spans);
        }

        [Fact]
        public async Task sample_test()
        {
            var result = await execute("/home/index");

            Assert.NotNull(result);
        }
    }
}