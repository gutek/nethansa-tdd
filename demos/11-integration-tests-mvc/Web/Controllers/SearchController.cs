﻿using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class SearchController : Controller
    {
        // GET
        public IActionResult Search(string q)
        {
            var model = new SearchQueryModel();
            return View(model);
        }
    }
}