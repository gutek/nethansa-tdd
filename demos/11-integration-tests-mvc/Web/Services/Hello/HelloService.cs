﻿using System.Threading;
using System.Threading.Tasks;

namespace Web.Services.Hello
{
    public interface IHelloService
    {
        string Hi(string name);
        string Hello(string name);
        Task<string> SlowHelloAsync(string name);
    }
    public class HelloService : IHelloService
    {
        // this is simple, we could easy go the same way
        // as Foaas or even decide to use DB to query
        // template in specific language passed in
        // Accept-Language Header

        public string Hi(string name)
        {
            return $"Hi {name}!";
        }
        public string Hello(string name)
        {
            return $"Hello, {name}!";
        }

        public async Task<string> SlowHelloAsync(string name)
        {
            await Task.Delay(2000);

            return Hello(name);
        }
    }
}