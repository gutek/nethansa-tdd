﻿using Xunit;

namespace lib.tests
{
    [Collection("DB_Access")]
    public class Test3
    {
        private readonly TestFixture _fixture;

        public Test3(TestFixture fixture)
        {
            System.Diagnostics.Debug.WriteLine("HELLO");
            _fixture = fixture;
        }
        [Fact]
        public void Test1()
        {
            Assert.Equal(10, _fixture.Age);
        }
    }
}