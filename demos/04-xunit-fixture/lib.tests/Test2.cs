﻿using Xunit;

namespace lib.tests
{
    [Collection("DB_Access")]
    public class Test2
    {
        private readonly TestFixture _fixture;

        public Test2(TestFixture fixture)
        {
            System.Diagnostics.Debug.WriteLine("HELLO");
            _fixture = fixture;
        }
        [Fact]
        public void Test1()
        {
            _fixture.Age += 2;
            Assert.Equal(12, _fixture.Age);
        }
    }
}