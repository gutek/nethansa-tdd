﻿using System;

namespace lib.tests
{
    public class TestFixture : IDisposable
    {
        public int Age { get; set; }
        public TestFixture()
        {
            // todo: add trace or log so you can show how many times its initialized
            Age = 10;
        }

        public void Dispose()
        {
        }
    }
}