﻿using Xunit;

namespace lib.tests
{
    public class SampleFixtureTest : IClassFixture<TestFixture>
    {
        private readonly TestFixture _fixture;

        public SampleFixtureTest(TestFixture fixture)
        {
            System.Diagnostics.Debug.WriteLine("HELLO");
            _fixture = fixture;
        }

        [Fact]
        public void Test1()
        {
            _fixture.Age += 2;
            Assert.Equal(12, _fixture.Age);
        }

        [Fact]
        public void Test2()
        {
            _fixture.Age += 1;
            Assert.Equal(11, _fixture.Age);
        }
        [Fact]
        public void Test3()
        {
            Assert.Equal(10, _fixture.Age);
        }
    }
}