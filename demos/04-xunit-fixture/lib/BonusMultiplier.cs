﻿using System;

namespace lib
{
    public static class BonusMultiplier
    {
        private static Func<decimal> _default = () => DateTime.Now.Day;
        private static Func<decimal> _current = _default;

        public static decimal Current
        {
            get
            {
                return _current();
            }
        }

        public static void _replace_for_tests(Func<decimal> newLogic)
        {
            _current = newLogic;
        }

        public static void _reset()
        {
            _current = _default;
        }
    }
}