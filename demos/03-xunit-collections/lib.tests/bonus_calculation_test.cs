﻿using System.Threading;
using Xunit;

namespace lib.tests
{
    [Collection("BonusMultipler")]
    public class bonus_calculation_test
    {
        [Fact]
        public void bonus_should_be_0()
        {
            BonusMultiplier._replace_for_tests(() => 0);
            var calc = new BonusCalculation();
            Thread.Sleep(100);
            var result = calc.GetTotalWithBonus(0);
            
            Assert.Equal(0, result);
            BonusMultiplier._reset();
        }


        [Fact]
        public void bonus_should_be_100()
        {
            BonusMultiplier._replace_for_tests(() => 1);
            var calc = new BonusCalculation();
            Thread.Sleep(10);
            var result = calc.GetTotalWithBonus(50);

            Assert.Equal(100, result);
            BonusMultiplier._reset();
        }


        [Fact]
        public void bonus_should_be_1001()
        {
            BonusMultiplier._replace_for_tests(() => 1);
            var calc = new BonusCalculation();
            Thread.Sleep(10);
            var result = calc.GetTotalWithBonus(50);

            Assert.Equal(100, result);
            BonusMultiplier._reset();
        }

        [Fact]
        public void bonus_should_be_1002()
        {
            BonusMultiplier._replace_for_tests(() => 1);
            var calc = new BonusCalculation();
            Thread.Sleep(10);
            var result = calc.GetTotalWithBonus(50);

            Assert.Equal(100, result);
            BonusMultiplier._reset();
        }
    }
}