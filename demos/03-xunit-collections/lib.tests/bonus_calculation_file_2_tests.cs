﻿using Xunit;

namespace lib.tests
{[Collection("BonusMultipler")]
    public class bonus_calculation_file_2_tests
    {
        [Fact]
        public void get_total_with_bonus_should_return_base_salary_when_multiplier_is_0()
        {
            // arrange
            BonusMultiplier._replace_for_tests(() => 0);
            var calc = new BonusCalculation();
            var baseSalary = 50m;
            
            // act
            var result = calc.GetTotalWithBonus(baseSalary);
            BonusMultiplier._reset();

            // assert
            Assert.Equal(baseSalary, result);
        }

        [Fact]
        public void get_total_with_bonus_should_return_100_when_multiplier_is_1()
        {
            // arrange
            BonusMultiplier._replace_for_tests(() => 1);
            var calc = new BonusCalculation();
            var baseSalary = 50m;
            
            // act
            var result = calc.GetTotalWithBonus(baseSalary);
            BonusMultiplier._reset();

            // assert
            Assert.Equal(baseSalary + 50, result);
        }

        [Fact]
        public void get_total_with_bonus_should_return_150_when_multiplier_is_2()
        {
            // arrange
            BonusMultiplier._replace_for_tests(() => 2);
            var calc = new BonusCalculation();
            var baseSalary = 50m;
            
            // act
            var result = calc.GetTotalWithBonus(baseSalary);
            BonusMultiplier._reset();

            // assert
            Assert.Equal(baseSalary + 100, result);
        }
    }
}