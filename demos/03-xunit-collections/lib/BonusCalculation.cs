﻿namespace lib
{
    public class BonusCalculation
    {
        public const decimal BaseBonus = 50;

        public decimal GetTotalWithBonus(decimal baseSalary)
        {
            var bonus = BaseBonus * BonusMultiplier.Current;

            return baseSalary + bonus;
        }
    }
}