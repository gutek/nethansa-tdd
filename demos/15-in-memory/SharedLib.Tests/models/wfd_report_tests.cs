﻿using SharedLib.Models;
using Xunit;

namespace SharedLib.Tests.models
{
    public class wfd_report_tests
    {
        private WfdReport _model;

        public wfd_report_tests()
        {
            _model = new WfdReport();
        }

        [Fact]
        public void when_properly_set_calculate_summary_should_return_0()
        {
            var result = _model.CalculateSummary();

            Assert.Equal(0m, result);
        }
    }
}