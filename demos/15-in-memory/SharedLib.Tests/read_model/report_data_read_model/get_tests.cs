﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Report.Data;
using SharedLib.ReadModel;
using Xunit;

namespace SharedLib.Tests.read_model.report_data_read_model
{
    public class get_tests : IDisposable
    {
        private ReportDbContext _context;
        private ReportDataReadModel _readModel;

        public get_tests()
        {
            var options = new DbContextOptionsBuilder<ReportDbContext>()
                .UseInMemoryDatabase(databaseName: "tests_in_mem_report_db")
                .Options;

            _context = new ReportDbContext(options);
            _readModel = new ReportDataReadModel(_context);
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

        /// <summary>
        /// Notice that I do not care about data, I care about not throwing or validating entries!!!
        /// </summary>
        [Fact]
        public async Task should_be_able_to_get_data_for_users()
        {
            var result = await _readModel.Get(Guid.Empty);
        }

        /// <summary>
        /// Notice that I do not care about data, I care about not throwing or validating entries!!!
        /// </summary>
        [Fact]
        public async Task should_throw_if_not_empty_guid_is_provided()
        {
            await Assert.ThrowsAsync<ArgumentException>(
                async () => await _readModel.Get(Guid.NewGuid())
            );
        }
    }
}