﻿using SharedLib.Extensions;
using Xunit;

namespace SharedLib.Tests.extensions
{
    public class is_not_null_or_withespace_tests
    {
        [Fact]
        public void should_return_false_when_string_is_empty()
        {
            var result = string.Empty.IsNotNullOrWhiteSpace();

            Assert.False(result);
        }
        [Fact]
        public void should_return_false_when_string_is_null()
        {
            string val = null;
            var result = val.IsNotNullOrWhiteSpace();

            Assert.False(result);
        }
        [Theory]
        [InlineData(" ")]
        [InlineData("  ")]
        [InlineData("\n")]
        [InlineData("\r")]
        [InlineData("\n\r")]
        public void should_return_false_when_string_is_whitespace(string val)
        {
            var result = val.IsNotNullOrWhiteSpace();

            Assert.False(result);
        }
        [Theory]
        [InlineData("a")]
        [InlineData("        a")]
        [InlineData("aa")]
        [InlineData(" aa")]
        [InlineData("sdfsdfsdf\n\r")]
        [InlineData("\raaa")]
        [InlineData("\n\raaaa")]
        public void should_return_true_when_string_is_not_empty_or_null(string val)
        {
            var result = val.IsNotNullOrWhiteSpace();

            Assert.True(result);
        }
    }
}