﻿using Microsoft.EntityFrameworkCore;
using Report.Data.Models;

namespace Report.Data
{
    public class ReportDbContext : DbContext
    {
        public ReportDbContext() : base()
        {
        }

        public ReportDbContext(DbContextOptions<ReportDbContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        /// <summary>
        /// This is important to allow inmemory testing
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // data seeding
            // modelBuilder.Entity<User>().HasData(new User() { });

            // call it before use of context.User -> context.Database.EnsureCreated()
        }
    }
}