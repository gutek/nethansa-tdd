﻿using System;

namespace SharedLib.Models
{
    /// <summary>
    /// Do not test POCO objects, do not test Props, Get/Set even if you want to null validation on set.
    ///
    /// There is however an exception, if POCO starts to have behaviour its not POCO anymore.
    ///
    /// Do test its behaviours.
    /// 
    /// THIS IS AN EXAMPLE OF Class with behaviour that should be tests - Calculate.
    /// </summary>
    public class WfdReport
    {
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }

        public decimal CalculateSummary()
        {
            // this is behaviour that should be unit tested

            return 0m;
        }
    }
}