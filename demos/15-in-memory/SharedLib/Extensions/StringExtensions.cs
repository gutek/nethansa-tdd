﻿namespace SharedLib.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrWhiteSpace(this string @this)
        {
            return string.IsNullOrWhiteSpace(@this);
        }
        public static bool IsNotNullOrWhiteSpace(this string @this)
        {
            return !@this.IsNullOrWhiteSpace();
        }
    }
}