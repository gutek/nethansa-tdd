﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Report.Data;
using SharedLib.Models;

namespace SharedLib.ReadModel
{
    public interface IReportDataReadModel
    {
        Task<IList<UserView>> Get(Guid empty);
    }

    public class ReportDataReadModel : IReportDataReadModel
    {
        private readonly ReportDbContext _context;

        /// <summary>
        /// IN dotnet core we can easily inject ReportDbContext, its ok to do it
        /// </summary>
        /// <param name="context"></param>
        public ReportDataReadModel(ReportDbContext context)
        {
            _context = context;
        }

        public async Task<IList<UserView>> Get(Guid empty)
        {
            if (empty != Guid.Empty)
            {
                throw new ArgumentException("wrong param", nameof(empty));
            }

            var data = await _context
                .Users
                .Where(x => x.AddressCity == "Wexford")
                .ToListAsync();

            return data.Select(x => new UserView
            {
                Address = $"{x.AddressCity}/{x.AddressZip}",
                Id = x.Id
            }).ToList();
        }
    }
}