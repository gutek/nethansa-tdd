﻿using lib.tests.conv.ReadModels;

namespace lib.tests.conv.ReadModels
{
    public class ClassReadModel : IReadModel
    {
    }

    public class ProperNameReadModel { }

    public class ProperClassReadModel : IReadModel { }
}

namespace lib.tests.conv
{
    public class ClassClassReadModel : IReadModel
    {
        
    }


}
