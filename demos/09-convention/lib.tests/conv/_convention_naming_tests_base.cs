﻿using System.Collections.Generic;
using System.Reflection;
using System;

namespace lib.tests.conv
{
    public class convention_naming_tests_base
    {
        protected readonly IEnumerable<Type> _types;

        public convention_naming_tests_base()
        {
            _types = GetAppTypes(Assembly.GetAssembly(typeof(convention_naming_tests_base)));
        }

        private static IEnumerable<Type> GetAppTypes(Assembly @this)
        {
            return @this.GetTypes();
        }
    }
}