﻿using System;
using System.Collections.Generic;
using System.Linq;
using lib.tests.conv.ReadModels;
using Xunit;

namespace lib.tests.conv
{
    public class read_model_conventions_tests : convention_naming_tests_base
    {
        [Fact]
        public void all_read_models_should_implement_ireadmodel_interface()
        {
            var readModelTypes = _types
                .Where(x => 
                    x.Namespace.IsNotNullOrEmpty()
                    && (
                            x.Name.Contains("ReadModel") 
                            || 
                            x.Namespace.Contains("ReadModel")
                    )
                );

            var results = new List<string>();

            foreach (var type in readModelTypes)
            {
                if (!type.IsAssignableTo<IReadModel>() && !type.IsAssignableTo<Exception>() && type.Name.Contains("ReadModel"))
                {
                    results.Add("{0} does not implement IReadModel Interface".FormatWith(type.Name));
                }
            }

            if (results.Count > 0)
            {
                Assert.True(false, "Following types does not implement IReadModel: {0}".FormatWith(string.Join(Environment.NewLine, results)));
            }
            else
            {
                Assert.True(true, "All types implement IReadModel interface");
            }
        }

        [Fact]
        public void all_read_models_should_ends_with_read_model_string()
        {
            var readModelTypes = _types.Where(x => x.Namespace.IsNotNullOrEmpty()
                && x.IsAssignableTo<IReadModel>());

            var results = new List<string>();

            foreach (var type in readModelTypes)
            {
                if (!type.Name.EndsWith("ReadModel"))
                {
                    results.Add("{0} does not end with ReadModel string".FormatWith(type.Name));
                }
            }

            if (results.Count > 0)
            {
                Assert.True(false, "Following types does not ends with ReadModel: {0}".FormatWith(string.Join(Environment.NewLine, results)));
            }
            else
            {
                Assert.True(true, "All types ends with ReadModel");
            }
        }

        [Fact]
        public void all_readmodels_should_be_in_readmodels_namespace()
        {
            var commands = _types.Where(x => x.Namespace.IsNotNullOrEmpty() && x.FullName.EndsWith("ReadModel"));

            var results = new List<string>();
            foreach (var type in commands)
            {
                if (!type.Namespace.Contains("ReadModels"))
                {
                    results.Add("{0} does not exists in ReadModels namespace".FormatWith(type.Name));
                }
            }

            if (results.Count > 0)
            {
                Assert.True(false, "Following read models does exists in ReadModels namespace: {0}".FormatWith(string.Join(Environment.NewLine, results)));
            }
            else
            {
                Assert.True(true, "All read models exists in ReadModels namespace");
            }
        }
    }
}