﻿using System;
using NSubstitute.Exceptions;

namespace lib.tests.conv
{
    public static class ExtensionMethods
    {
        public static string FormatWith(this string @this, params object[] args)
        {
            return string.Format(@this, args);
        }

        public static bool IsNotNullOrEmpty(this string @this)
        {
            return !string.IsNullOrEmpty(@this);
        }

        public static bool IsAssignableTo<T>(this Type @this)
        {
            return typeof(T).IsAssignableFrom(@this);
        }
        
    }
}