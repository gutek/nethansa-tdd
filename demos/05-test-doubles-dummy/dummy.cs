﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace lib.tests
{
    public interface IShape
{
    int A { get; }
    int Area();
}

public class DummyShape : IShape
{
    public DummyShape()
    {
    }

    public A { get; }
    public int Area() 
    {
        return 0;
    }
}
}