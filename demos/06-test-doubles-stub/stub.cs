﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace lib.tests
{
    public interface IMailService
    {
        bool Send(string email);
    }

    public StubMailService : IMailService
    {
        public bool Send(string email)
        {
            if(email != null) 
            {
                return true;
            }
            return false;
        }
    }
}