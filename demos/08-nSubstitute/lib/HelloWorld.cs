﻿namespace lib
{
    public interface IEmployee
    {
        string SayHi(string name);
    }

    public class HelloWorld
    {
        public string Hello(IEmployee emp)
        {
            return $"Hello, {emp.SayHi("test")}";
        }
    }
}