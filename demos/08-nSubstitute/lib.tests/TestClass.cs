﻿using NSubstitute;
using Xunit;

namespace lib.tests
{
    public class TestClass
    {
        private HelloWorld _helloWorld;

        public TestClass()
        {
            _helloWorld = new HelloWorld();
        }

        [Fact]
        public void Test1()
        {
            // arrange
            var emp = Substitute.For<IEmployee>();

            // act
            var result = _helloWorld.Hello(emp);

            // assert
            Assert.Equal("Hello, ", result);
        }

        [Fact]
        public void Test2()
        {
            // arrange
            var name = "Maciek";
            var emp = Substitute.For<IEmployee>();
            
            emp.SayHi(Arg.Any<string>())
                .Returns(name);

            // act
            var result = _helloWorld.Hello(emp);

            // assert
            Assert.Equal($"Hello, {name}", result);
        }
        [Fact]
        public void Test3()
        {
            // arrange
            var name = "Maciek";
            var emp = Substitute.For<IEmployee>();
            
            emp.SayHi(Arg.Any<string>())
                .Returns(name);

            // act
            var result = _helloWorld.Hello(emp);

            emp.Received(2).SayHi(Arg.Any<string>());

            // assert
            //Assert.Equal($"Hello, {name}", result);
        }
        [Fact]
        public void Test4()
        {
            // arrange
            var name = "Maciek";
            var emp = Substitute.For<IEmployee>();
            
            emp.SayHi(Arg.Is(name))
                .Returns(name);

            // act
            var result = _helloWorld.Hello(emp);

            emp.Received(1).SayHi(Arg.Is(name));

            // assert
            //Assert.Equal($"Hello, {name}", result);
        }
    }
}