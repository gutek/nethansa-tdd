﻿using System;
using System.Security.Cryptography;

namespace Web.Services.CurrentUser
{
    public interface ICurrentUserService
    {
        User Current();
    }
    public class CurrentUserService : ICurrentUserService
    {
        public User Current()
        {
            var user = new User();
            
            var rnd = new Random();

            user.Age  = rnd.Next(36);
            user.FirstName = "Grzegorz";
            user.LastName = "Brzęczyszczykiewicz";

            return user;
        }
    }

    public class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age{ get; set; }
    }
}