﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace Web.Tests.magic
{
    public class mvc_sample_tests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public mvc_sample_tests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task should_return_status_code_200()
        {
            var client = _factory.CreateClient();
            
            var result = await client.GetAsync("/home/index");

            result.EnsureSuccessStatusCode();
        }
    }
}