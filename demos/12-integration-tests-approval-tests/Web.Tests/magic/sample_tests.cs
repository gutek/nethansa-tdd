﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Web.Tests.Helpers;
using Xunit;

namespace Web.Tests.magic
{
    public class sample_tests
    {
        private TestServer _server;
        private HttpClient _client;

        public sample_tests()
        {
            _server = new TestServer(new WebHostBuilder()
                .UseContentRoot(@"D:\workshops\workshops\demos\30\Web")
                .UseEnvironment("Development")
                .UseStartup<Startup>()
            );

            _client = _server.CreateClient();
        }

        private async Task<string> execute(string path)
        {
            var result = await _client.GetAsync(path);
            var content = await result.Content.ReadAsStringAsync();

            return content;
        }

        [Fact]
        public async Task sample_test()
        {
            var result = await execute("/home/index");

            var verify = MyApprovalTests.Verify(result, "home_index.txt");
            Assert.True(verify);
        }
    }
}