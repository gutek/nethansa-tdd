﻿using System.IO;
using System.Reflection;

namespace Web.Tests.Helpers
{
    public class MyApprovalTests
    {
        public static bool Verify(string actual, string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();

            var path = Path.GetDirectoryName(assembly.Location);
            var assemblyName = assembly.GetName().Name;

            path = path.Substring(0, path.IndexOf(assemblyName) + assemblyName.Length);
            path = Path.Combine(path, "Approvals");

            var filePath = Path.Combine(path, Path.ChangeExtension(fileName, "txt"));

            if (File.Exists(filePath))
            {
                var excpected = File.ReadAllText(filePath);
                return excpected == actual;
            }

            File.WriteAllText(filePath, actual);

            // fail first
            return false;
        }
    }
}