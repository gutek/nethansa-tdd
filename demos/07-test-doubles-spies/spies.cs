﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace lib.tests
{
    public class Service
    {
        public bool WasCalled;
        public int CallConut;
        public int OddCount;

        public bool Act(int num) 
        {
            WasCalled = true;
            CallConut++;
            if(num % 2 == 0) 
            {
                return false;
            }

            // magic

            OddCount++;
            return true;
        }
    }
}