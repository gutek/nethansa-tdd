﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Web.BackgroundServices
{
    public class SimpleBackgroundService : IHostedService
    {
        private readonly ILogger<SimpleBackgroundService> _log;

        public SimpleBackgroundService(ILogger<SimpleBackgroundService> log)
        {
            _log = log;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _log.LogInformation("--------------START W SIMPLE");

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _log.LogInformation("--------------STOP W SIMPLE");
            return Task.CompletedTask;
        }
    }




















    public class SimpleBackgroundService1 : IHostedService, IDisposable
    {
        private readonly ILogger<SimpleBackgroundService> _log;
        private readonly IServiceProvider _provider;
        private Timer _time;

        public SimpleBackgroundService1(ILogger<SimpleBackgroundService> log
            , IServiceProvider provider)
        {
            _log = log;
            _provider = provider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _time = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(2));
            _log.LogInformation("--------------START W SIMPLE");

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            _log.LogInformation("--------------DO WORK");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _log.LogInformation("--------------STOP W SIMPLE");
            _time?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _time?.Dispose();
        }
    }
}