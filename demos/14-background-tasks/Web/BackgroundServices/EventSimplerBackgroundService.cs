﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Web.BackgroundServices
{
    public class EventSimplerBackgroundService : BackgroundService
    {
        private readonly ILogger<EventSimplerBackgroundService> _log;

        public EventSimplerBackgroundService(ILogger<EventSimplerBackgroundService> log)
        {
            _log = log;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _log.LogInformation("=======FROM really simple background worker");
            return Task.CompletedTask;
        }
    }
}