﻿using System;
using System.Collections;
using System.Collections.Generic;
using FluentAssertions;
using Shouldly;
using Xunit;

namespace lib.tests
{
    public class Calculator
    {
        public int Add(int x, int y) => x + y;
        public int Div(int x, int y)
        {
            if (y == 0)
            {
                throw new Exception("pamiętaj cholero, nigdy nie dziel przez zero!");
            }

            return x / y;
        }

        public object Get(int a) => a % 2 == 0 ? new Calculator() : null;
    }

    public class calcutator_tests_fact
    {
        private readonly Calculator _calc;

        public calcutator_tests_fact()
        {
            _calc = new Calculator();
        }

        [Fact]
        public void normal_x_unit_assertions()
        {
            // act
            var result20 = _calc.Add(10, 10);
            var nullCalc = _calc.Get(1);
            var notNullCalc = _calc.Get(2);
            
            Assert.Equal(20, result20);
            var ex = Assert.Throws<Exception>(() => _calc.Div(1, 0));
            Assert.NotNull(notNullCalc);
            Assert.Null(nullCalc);
            var localCalc = Assert.IsType<Calculator>(notNullCalc);
        }
        [Fact]
        public void shouldly_x_unit_assertions()
        {
            // act
            var result20 = _calc.Add(10, 10);
            var nullCalc = _calc.Get(1);
            var notNullCalc = _calc.Get(2);
            
            result20.ShouldBe(20);
            var ex = Should.Throw<Exception>(() => _calc.Div(1, 0));

            nullCalc.ShouldBeNull();
            notNullCalc.ShouldNotBeNull();

            var localCalc = notNullCalc.ShouldBeOfType<Calculator>();
        }
        [Fact]
        public void fluent_x_unit_assertions()
        {
            // act
            var result20 = _calc.Add(10, 10);
            var nullCalc = _calc.Get(1);
            var notNullCalc = _calc.Get(2);
            
            result20.Should().Be(20);
            _calc.Invoking(x => x.Div(1, 0))
                .Should()
                .Throw<Exception>()
                .WithMessage("");

            nullCalc.Should().BeNull();
            notNullCalc.Should().NotBeNull();

            var localCalc = notNullCalc.Should().BeOfType<Calculator>();
        }
    }
}