﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace lib.tests
{
    public class calcutator_tests_theory : IDisposable
    {
        public calcutator_tests_theory () 
        {
            // executed per every tests, initialization
        }

        public void Dispose() 
        {
            // executed per every tesets, cleanup
        }

        [Theory]
        [InlineData(5,1,4)]
        [InlineData(7,3,4)]
        public void adding_two_numers_should_returns_sum(int expect, int a, int b)
        {
            // arrange

            // act
            var sum = a + b;

            // assert
            Assert.Equal(expect, sum);
        }

        public static IEnumerable<object[]> TestData = new List<object[]>()
        {
            new object[] {2, 4, 1},
            new object[] {5, 6, 1}
        };

        [Theory]
        [MemberData(nameof(TestData))]
        [ClassData(typeof(DataGenerator))]
        [ClassData(typeof(DataGenerator1))]
        public void should_substrat_two_numbers(int expect, int a, int b)
        {
            // arrange

            // act
            var sum = a - b;

            // assert
            Assert.Equal(expect, sum);
        }
        //}

        

    }
    public class DataGenerator1 : List<object[]>
    {
        private readonly IList<object[]> _data = new List<object[]>
        {
            new object[] {3, 4, 1},
            new object[] {5, 6, 1}
        };

        public DataGenerator1()
        {
            AddRange(_data);
        }
    }
        public class DataGenerator : IEnumerable<object[]>
    {
        private readonly IList<object[]> _data = new List<object[]>
        {
            new object[] {3, 4, 1},
            new object[] {8, 6, 1}
        };

        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    }
}