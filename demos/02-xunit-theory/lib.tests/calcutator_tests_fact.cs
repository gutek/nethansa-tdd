﻿using System;
using System.Collections;
using System.Collections.Generic;
using Xunit;

namespace lib.tests
{
    public class calcutator_tests_fact : IDisposable
    {
        public calcutator_tests_fact () 
        {
            // executed per every test, initialization
        }

        public void Dispose() 
        {
            // executed per every test, cleanup
        }

        [Fact(Skip="Reason")]
        public void adding_1_and_1_should_return_2()
        {
            // test
        }

        [Fact]
        public void adding_minus_1_and_minus_2_should_return_minus_3() 
        {
            // test
        }
    }
}