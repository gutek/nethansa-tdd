﻿/*
   The MIT License (MIT)
   
   Copyright (c) 2015 Igor Kulman
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
  
   https://github.com/igorkulman/FOAASClient
 */

using System.Threading.Tasks;

namespace Web.Services.Foaas
{
    public interface IFoaasClient
    {
        Task<Response> Off(string name, string from);
        Task<Response> You(string name, string from);
        Task<Response> That(string from);
        Task<Response> This(string from);
        Task<Response> Everything(string from);
        Task<Response> Everyone(string from);
        Task<Response> Donut(string name, string from);
        Task<Response> Shakespeare(string name, string from);
        Task<Response> Linus(string name, string from);
        Task<Response> King(string name, string from);
        Task<Response> Pink(string from);
        Task<Response> Life(string from);
        Task<Response> Chainsaw(string name, string from);
        Task<Response> Outside(string name, string from);
        Task<Response> Thing(string thing, string from);
        Task<Response> Thanks(string from);
        Task<Response> Flying(string from);
        Task<Response> Fascinating(string from);
        Task<Response> Madison(string name, string from);
        Task<Response> Cool(string from);
        Task<Response> Field(string name, string from, string reference);
        Task<Response> Nugget(string name, string from);
        Task<Response> Yoda(string name, string from);
        Task<Response> Ballmer(string name, string company, string from);
        Task<Response> What(string from);
        Task<Response> Because(string from);
        Task<Response> Caniuse(string tool, string from);
        Task<Response> Bye(string from);
        Task<Response> Diabetes(string from);
        Task<Response> Bus(string name, string from);
        Task<Response> Xmas(string name, string from);
        Task<Response> Bday(string name, string from);
        Task<Response> Awesome(string from);
        Task<Response> Tucker(string from);
        Task<Response> Bucket(string from);
        Task<Response> Family(string from);
        Task<Response> Shutup(string name, string from);
        Task<Response> Zayn(string from);
        Task<Response> KeepCalm(string reaction, string from);
        Task<Response> DoSomething(string todo, string something, string from);
        Task<Response> Mornin(string from);
        Task<Response> Thumbs(string who, string from);
        Task<Response> Retard(string from);
        Task<Response> Greed(string noun, string from);
        Task<Response> Anyway(string company, string from);
        Task<Response> Back(string name, string from);
        Task<Response> Bag(string from);
        Task<Response> Blackadder(string name, string from);
        Task<Response> BravoMike(string name, string from);
        Task<Response> Cocksplat(string name, string from);
        Task<Response> Dalton(string name, string from);
        Task<Response> Deraadt(string name, string from);
        Task<Response> GolfFoxtrotYankee(string name, string from);
        Task<Response> Give(string from);
        Task<Response> Horse(string from);
        Task<Response> FuckingOff(string name, string from);
        Task<Response> Keep(string name, string from);
        Task<Response> Look(string name, string from);
        Task<Response> Looking(string from);
        Task<Response> Maybe(string from);
        Task<Response> Me(string from);
        Task<Response> No(string from);
        Task<Response> OffWith(string behaviour, string from);
        Task<Response> Particular(string obj, string from);
        Task<Response> Problem(string name, string from);
        Task<Response> Pulp(string language, string from);
        Task<Response> Ridiculous(string from);
        Task<Response> ReadTheManual(string from);
        Task<Response> Sake(string from);
        Task<Response> Shit(string from);
        Task<Response> Single(string from);
        Task<Response> Think(string name, string from);
        Task<Response> Thinking(string name, string from);
        Task<Response> Too(string from);
        Task<Response> Zero(string from);
    }
}
