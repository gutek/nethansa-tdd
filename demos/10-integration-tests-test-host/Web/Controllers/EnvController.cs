﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Web.Controllers
{
    public class EnvController : Controller
    {
        private readonly IConfiguration _config;

        public EnvController(IConfiguration config)
        {
            _config = config;
        }

        public IActionResult Info()
        {
            var name = _config["app_name"];
            return View("info", name);
        }
    }
}