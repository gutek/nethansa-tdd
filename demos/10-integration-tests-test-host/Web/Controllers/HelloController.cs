﻿using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    public class HelloController : Controller
    {
        public IActionResult Hi(string name)
        {
            // yep, with string we need to specify view name
            return View("Hi", name);
        }
        public IActionResult Hi()
        {
            // yep, with string we need to specify view name
            return View("Hi", "BAD BOY!");
        }
    }
}