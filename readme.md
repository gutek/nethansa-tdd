# dotnet tdd

Materiały szkoleniowe dot. szkolenia TDD przeprowadzonego w dniach 2019-04-15/16.

## STRUKTURA

* **exercises** - zbiór wszystki ćwiczeń jakie były wykonywane w trakcie trwania drugiego dnia szkolenia. Ćwiczenia są tutaj w oryginalnej formie, nie zmienione, nie rozwiązane.
* **solutions** - rozwiązane (niektóre) ćwiczenia z _exercises_.
* **samples** - zbiór przykładów, które były prezentowane w trakcie trwania warsztatów.
* **demos** - zbiór zbiór dem.
* **slides** - slajdy z warsztatów w wersji offline, link do wersji online ponzej.

## ANKIETA

* [ankieta](http://bit.ly/nethansa-ankieta)

## LINKI

### PREZENTACJE

* [wprowadzenie do testowania](https://slides.com/gutek/nethansa-tdd?token=FcbfKT9c)

### REFACTORING

* [Refactoring poprzez TDD - Marcin Malinowski](https://github.com/orient-man/TripServiceKata)
* [Recactoring za pomocą TDD (miał być uncle bob ale to ktoś inny jednak robił)](https://www.youtube.com/watch?v=_NnElPO5BU0)

### KONWENCJA

* [Code Foresting - Sebastian Gębski](https://www.youtube.com/watch?v=0_FvZ3zkTQ4&t=0s&list=PL8BUDiR2Y8YvhzYYxoetZ3PWzXK3CJwtu&index=33)